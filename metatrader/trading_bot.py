import pytz
import pandas as pd
import MetaTrader5 as mt5
import time
from datetime import datetime
from threading import Timer
from datetime import timedelta
import math
from ta.volatility import BollingerBands
from ta.momentum import StochRSIIndicator
from ta.volume import volume_weighted_average_price
from ta.trend import PSARIndicator
from ta.trend import ema_indicator
from ta.trend import macd_diff
# display data on the MetaTrader 5 package
print("MetaTrader5 package author: ",mt5.__author__)
print("MetaTrader5 package version: ",mt5.__version__)

# Establish connection to the MetaTrader 5 terminal
if not mt5.initialize():
    print("initialize() failed, error code =", mt5.last_error())
    quit()

server_name = "Add server name here"
server_num = "Add account number here"
account = "Add account number here"
password = "add account password here"

ea_magic_number = 9986189 # if you want to give every bot a unique identifier

authorized=mt5.login(account)  # the terminal database password is applied if connection data is set to be remembered


def get_info_tick(symbol):
    '''https://www.mql5.com/en/docs/integration/python_metatrader5/mt5symbolinfotick_py
    '''

    # get symbol tick info
    info = mt5.symbol_info_tick(symbol)
    print("initialize() failed, error code =",mt5.last_error())
    return info


# Set global variable emergency stop loss (if last 2 trades are stopped out)
esl_bool = False
esl_cooldown_bool = False
esl_counter = -1
#esl_time = ""
e_sl_switch = False
symbol = 'EURUSD'


tick = get_info_tick(symbol)

def get_info(symbol):
    '''https://www.mql5.com/en/docs/integration/python_metatrader5/mt5symbolinfo_py
    '''
    # get symbol properties
    info=mt5.symbol_info(symbol)
    return info

def get_info_tick(symbol):
    '''https://www.mql5.com/en/docs/integration/python_metatrader5/mt5symbolinfotick_py
    '''

    # get symbol tick info
    info = mt5.symbol_info_tick(symbol)
    print("initialize() failed, error code =",mt5.last_error())
    return info


def check_esl(df):
    # input is historical trades
    comment_df = df['comment'] != "sent by python"
    comment_df = df[comment_df]

    # Get the last two trades profit/loss
    profits = comment_df.tail(2)['profit'].values
    if profits[0] < 0 and profits[1] < 0:
        return True
    else:
        return False

def check_esl_1(df):
    # input is historical trades
    comment_df = df['comment'] != "sent by python"
    comment_df = df[comment_df]

    # Get the last two trades profit/loss
    profits = comment_df.tail(2)['profit'].values
    if profits[0] < 0:
        return True
    else:
        return False


def open_trade(action, symbol, lot, tp, sl, deviation):
    '''https://www.mql5.com/en/docs/integration/python_metatrader5/mt5ordersend_py
    '''
    # prepare the buy request structure
    symbol_info = get_info(symbol)
    point = mt5.symbol_info(symbol).point
    spread = symbol_info.spread*point
    if action == 'BUY':
        trade_type = mt5.ORDER_TYPE_BUY
        price = mt5.symbol_info_tick(symbol).ask
        sl = price - sl * 10 * point - spread
        tp =  price + tp * 10 * point + spread
    elif action =='SELL':
        trade_type = mt5.ORDER_TYPE_SELL
        price = mt5.symbol_info_tick(symbol).bid
        sl = price + sl * 10 * point + spread
        tp =  price - tp * 10 * point - spread

    buy_request = {
        "action": mt5.TRADE_ACTION_DEAL,
        "symbol": symbol,
        "volume": lot,
        "type": trade_type,
        "price": price,
        "sl": sl,
        "tp": tp,
        "deviation": deviation,
        "magic": ea_magic_number,
        "comment": "sent by python",
        "type_time": mt5.ORDER_TIME_GTC, # good till cancelled
        "type_filling": mt5.ORDER_FILLING_IOC,
    }
    # send a trading request
    result = mt5.order_send(buy_request)        
    return result, buy_request 

def close_trade(action, buy_request, result, deviation):
    '''https://www.mql5.com/en/docs/integration/python_metatrader5/mt5ordersend_py
    '''
    # create a close request
    symbol = buy_request['symbol']
    if action == 'BUY':
        trade_type = mt5.ORDER_TYPE_BUY
        price = mt5.symbol_info_tick(symbol).ask
    elif action =='SELL':
        trade_type = mt5.ORDER_TYPE_SELL
        price = mt5.symbol_info_tick(symbol).bid
    position_id=result.order
    lot = buy_request['volume']

    close_request={
        "action": mt5.TRADE_ACTION_DEAL,
        "symbol": symbol,
        "volume": lot,
        "type": trade_type,
        "position": position_id,
        "price": price,
        "deviation": deviation,
        "magic": ea_magic_number,
        "comment": "python script close",
        "type_time": mt5.ORDER_TIME_GTC, # good till cancelled
        "type_filling": mt5.ORDER_FILLING_IOC,
    }
    # send a close request
    result=mt5.order_send(close_request)

    return result, close_request


def get_historical_trades(symbol, from_time):
    '''https://www.mql5.com/en/docs/integration/python_metatrader5/mt5historydealsget_py
    '''

    timezone = pytz.timezone("EET")
    # create 'datetime' object in EET time zone to avoid the implementation of a local time zone offset
    eet_to = datetime.now(timezone)
    # INPUTS
    # symbol_group = "*EUR*"
    # from_time = datetime.datetime object
    # get deals for symbols whose names contain neither "EUR" nor "GBP"

    if "EUR" in symbol:
        group_symbol="*EUR*"
    elif "CHF" in symbol:
        group_symbol="*CHF*"
    elif "AUD" in symbol:
        group_symbol="*AUD*"
    
    deals = mt5.history_deals_get(from_time, eet_to, group=group_symbol)
    if deals == None:
        print("No deals, error code={}".format(mt5.last_error()))
        return None
    elif len(deals) > 0:
        # display these deals as a table using pandas.DataFrame
        df=pd.DataFrame(list(deals),columns=deals[0]._asdict().keys())
        #df['time'] = pd.to_datetime(df['time'], unit='m')
        df_time = df['time'].to_numpy()
        dftime = []
        for i in range(len(df_time)):
            dftime.append(datetime.fromtimestamp(df_time[i], timezone).strftime("%Y-%m-%d %H:%M"))
        x = pd.Series(dftime)
        df['time'] = x
        df.set_index('time', drop=True, inplace=True)

        # Filter dataframe
        df = df[['ticket', 'order', 'type', 'volume', 'price', 
            'profit', 'symbol', 'comment']]

        return df

def ema_trend(close_data, window):

    ema = ema_indicator(close_data, window=window, fillna=False).dropna()

    results = []

    for i in range(1, len(ema)):
        if ema.iloc[i-1] < ema.iloc[i]:
            results.append("UPTREND")
        elif ema.iloc[i-1] > ema.iloc[i]:
            results.append("DOWNTREND")
        else:
            results.append("SIDEWAYS")
    
    return results

def ema_trendv2(close_data, window):

    ema = ema_indicator(close_data, window=window, fillna=False).dropna()

    results = []

    for i in range(2, len(ema)):
        if ema.iloc[i-2] < ema.iloc[i-1] < ema.iloc[i]:
            results.append("UPTREND")
        elif ema.iloc[i-2] > ema.iloc[i-1] > ema.iloc[i]:
            results.append("DOWNTREND")
        else:
            results.append("SIDEWAYS")
    
    return results
# ---------------------------------------------------------------------------- #
#       TRIPLE THREAT STRATEGY: STOCH_RSI+MACD_HISTOGRAM+BOLLINGER_BANDS V2    #
# ---------------------------------------------------------------------------- #

def stochrsi_macd_bollinger_indicatorv2(df, b_band_delta):
    # THIS STRATEGY IS A CONTINUATION TREND STRATEGY.
    # IT DOES NOT TRY TO 'CHASE' THE TOPS AND BOTTOMS LIKE
    # MOST STRATEGIES.

    close_data = df['close']
    #close_data = close_data.iloc[200:]

    # trend analysis
    #trend = ema_ribbon_trend(close_data)
    #trend = ema_trend(close_data, 10)
    trend = ema_trendv2(close_data, 2)
    macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()
    hband = BollingerBands(close_data, 20, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 20, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 20, 3, False).bollinger_mavg()
    min_list = min([len(stoch_rsi_d), len(hband), len(macd_histogram), len(close_data), len(trend)])

    df_times = df.index.to_numpy()
    df_times = pd.Series(df_times).tail(min_list).to_list()

    close_data = close_data.tail(min_list).to_list()
    macd_histogram = macd_histogram.tail(min_list).to_list()
    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    # this gives better results for some reason?
    trend = trend[:min_list]
    #trend = trend[-min_list:]
    # ---------------------------------------------------------------------------- #
    #                                     LISTS                                    #
    # ---------------------------------------------------------------------------- #

    series_list = [len(trend), len(lband), len(hband), len(mband), len(stoch_rsi_d),
        len(macd_histogram), len(close_data), len(df_times)]

    lband_len = len(lband) - min(series_list)
    trend_len = len(trend) - min(series_list)
    macd_len = len(macd_histogram) - min(series_list)
    stoch_len = len(stoch_rsi_d) - min(series_list)
    close_len = len(close_data) - min(series_list)
    times_len = len(df_times) - min(series_list)
    if lband_len > 0:
        hband = hband[lband_len:]
        lband = lband[lband_len:]
        mband = mband[lband_len:]

    if trend_len > 0:
        trend = trend[trend_len:]
    if macd_len > 0:
        macd_histogram = macd_histogram[macd_len:]
    if close_len > 0:
        close_data = close_data[close_len:]
    if stoch_len > 0:
        stoch_rsi_d = stoch_rsi_d[stoch_len:]
    if times_len > 0:
        df_times = df_times[times_len:]

    test_list = []
    results_list = []
    for i in range(min_list):
        #trend =
        # first do something
        delta_lband = abs(lband[i] - close_data[i])
        delta_hband = abs(hband[i] - close_data[i])
        delta_mband = abs(mband[i] - close_data[i])
        #print("delta_lband: %s" % delta_lband)
        #print("delta_hband: %s" % delta_hband)
        #print("delta_mband: %s" % delta_mband)
        # FIX THE DELTAS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # check ema trend, if uptrend-> look for buys etc..
        if trend[i] == "UPTREND":
            # check for pullback during uptrend
            if delta_mband <= b_band_delta:
                if 0 < macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5:
                    # pullback BUY
                    results_list.append("BUY")
                    test_list.append(("LONG", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    test_list.append(("IGNORE", df_times[i], close_data[i]))
                    continue
            elif lband[i] < close_data[i] < mband[i]:
                if macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5: 
                    # pullback BUY
                    results_list.append("BUY")
                    test_list.append(("BUY", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    test_list.append(("IGNORE", df_times[i], close_data[i]))
                    continue
            else:
                results_list.append("IGNORE")
                continue

        elif trend[i] == "DOWNTREND":
            # check for potential sells during pullback
            asdf = delta_mband
            if delta_mband <= b_band_delta:
                if -0.2 < macd_histogram[i] < 0 and 0.3 <= stoch_rsi_d[i]:
                    # pullback SELL
                    results_list.append("SELL")
                    test_list.append(("SHORT", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    test_list.append(("IGNORE", df_times[i], close_data[i]))
                    continue                    
            else:
                results_list.append("IGNORE")
                continue
        else:
            # SIDEWAYS MARKET
            # check macd and stochrsi here!
            
            if delta_hband <= b_band_delta:
                # check macd and stochrsi for sell
                if macd_histogram[i] >= 0 and stoch_rsi_d[i] > 0.5:
                    results_list.append("SELL")
                    test_list.append(("SHORT", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    continue
            elif delta_lband <= b_band_delta:
                if macd_histogram[i] <= 0 and stoch_rsi_d[i] < 0.5:
                    results_list.append("BUY")
                    test_list.append(("LONG", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    continue
    # returns a list with the oldest data first.
    return results_list


# ---------------------------------------------------------------------------- #
#                      MACD HISTOGRAM WITH STOCHASTIC RSI                      #
# ---------------------------------------------------------------------------- #

def check_macd_with_stochrsi(df):
    # data = pandas dataframe of all indicators including close price
    # window = range/bars to look
    # rsi_delta = the error term when looking at the rsi levels.
    # between close price and ema
    # checks for stochastic crossover while above 0.8 or below 0.2
    close_data = df['close']
    # Steps:
    # 1. Check if ema is uptrend or downtrend.
    # 2. if so, look for when stochrsi <= 0.2 + rsi_delta
    # 3. confirm move with MACD histogram (diff).
    macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
    macd_histogram = macd_histogram.dropna()
    ema = ema_indicator(close_data, window=50, fillna=False).dropna()
    # Use stoch_rsi_d to search for divergences and find entry points
    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()

    #print(stoch_rsi_d)
    #ema = pd.DataFrame(ema)
    min_list = min([len(stoch_rsi_d), len(ema), len(macd_histogram), len(close_data)])
    # ---------------------------------------------------------------------------------------------------------
    close_data = close_data.tail(min_list).to_list()
    macd_histogram = macd_histogram.tail(min_list).to_list()
    ema = ema.tail(min_list).to_list()
    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()

    window_list = []

    for i in range(1, min_list):

        #first check if ema is uptrend or downtrend
        if math.exp(ema[i]-ema[i-1]) < 1:
            # if ema is increasing
            if stoch_rsi_d[i] >= 0.6:
                if macd_histogram[i] >= 0.05:
                    window_list.append("SELL")
                else:
                    window_list.append("IGNORE")
            elif stoch_rsi_d[i] <= 0.4:
                if macd_histogram[i] <= -0.05:
                    window_list.append("BUY")
                else:
                    window_list.append("IGNORE")

            else:
                window_list.append("IGNORE")

        elif math.exp(ema[i]-ema[i-1]) > 1:
            # if ema is decreasing
            if stoch_rsi_d[i] >= 0.6:
                if macd_histogram[i] >= 0.05:
                    window_list.append("SELL")
                else:
                    window_list.append("IGNORE")
            elif stoch_rsi_d[i] <= 0.4:
                if macd_histogram[i] <= -0.05:
                    window_list.append("BUY")
                else:
                    window_list.append("IGNORE")

            else:
                window_list.append("IGNORE")
        else:
            window_list.append("IGNORE")

    return window_list

# ---------------------------------------------------------------------------- #
#                        SIMPLE BOLLINGER BANDS STRATEGY 2                     #
# ---------------------------------------------------------------------------- #

def simple_bollinger_strategy_2(df, bb_delta=0.0001):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    trend = ema_trend(close_data, 50)

    hband = BollingerBands(df['close'], 50, 3, False).bollinger_hband().dropna()
    lband = BollingerBands(df['close'], 50, 3, False).bollinger_lband().dropna()
    mband = BollingerBands(df['close'], 50, 3, False).bollinger_mavg().dropna()
    psar_d = PSARIndicator(df['high'], df['low'], df['close']).psar_down().dropna()
    psar_u = PSARIndicator(df['high'], df['low'], df['close']).psar_up().dropna()

    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()

    min_list = min([len(hband), len(psar_d), len(psar_u), len(trend), len(close_data), len(stoch_rsi_d)])

    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()

    trend = trend[-min_list:]
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()





    results = []

    for idx in range(len(hband)):

        delta_lband = abs(lband[idx] - close_data[idx])
        delta_hband = abs(hband[idx] - close_data[idx])
        delta_mband = abs(mband[idx] - close_data[idx])

        delta_high_hband = high_data[idx] - hband[idx]
        delta_low_lband = low_data[idx] - lband[idx]

        if trend[idx] == "DOWNTREND":
            if delta_hband < delta_mband or delta_high_hband <= bb_delta:
                results.append("SELL")
                print('sell')
            else:
                results.append("IGNORE")

        elif trend[idx] == "UPTREND":
            if delta_lband < delta_mband or delta_low_lband <= bb_delta:
                results.append("BUY")
                print('buy')
            else:
                results.append("IGNORE")
        
        else:
            results.append("IGNORE")

        return results

# ---------------------------------------------------------------------------- #
#                        SIMPLE BOLLINGER BANDS STRATEGY 3                     #
# ---------------------------------------------------------------------------- #

def simple_bollinger_strategy_3(df):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    trend = ema_trend(close_data, 50)

    hband = BollingerBands(df['close'], 50, 3, False).bollinger_hband().dropna()
    lband = BollingerBands(df['close'], 50, 3, False).bollinger_lband().dropna()
    mband = BollingerBands(df['close'], 50, 3, False).bollinger_mavg().dropna()
    psar_d = PSARIndicator(df['high'], df['low'], df['close']).psar_down_indicator().dropna()
    psar_u = PSARIndicator(df['high'], df['low'], df['close']).psar_up_indicator().dropna()

    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()

    min_list = min([len(hband), len(psar_d), len(psar_u), len(trend), len(close_data), len(stoch_rsi_d)])

    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()

    trend = trend[-min_list:]
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()
    psar_d = psar_d.tail(min_list).to_list()
    psar_u = psar_u.tail(min_list).to_list()




    results = []

    for idx in range(len(hband)):

        delta_lband = abs(lband[idx] - close_data[idx])
        delta_hband = abs(hband[idx] - close_data[idx])
        delta_mband = abs(mband[idx] - close_data[idx])

        if trend[idx] == "DOWNTREND":
            if delta_hband < delta_mband and psar_d[idx] == 1.0:
                results.append("SELL")
                print('sell')
            else:
                results.append("IGNORE")

        elif trend[idx] == "UPTREND":
            if delta_lband < delta_mband and psar_u[idx] == 1.0:
                results.append("BUY")
                print('buy')
            else:
                results.append("IGNORE")
        
        else:
            results.append("IGNORE")

        return results



# ---------------------------------------------------------------------------- #
#                          DOUBLE VWAP BOLLINGER BANDS                         #
# ---------------------------------------------------------------------------- #

def vwap_bollinger_1(df, b_band_delta=0.0001):
    # optional vwap windows; 1440, 720, 360
    vwap_band = volume_weighted_average_price(df['high'], df['low'], 
        df['close'], df['tick_volume'], window=2880, fillna=True)
    vwap_band_upper_1 = vwap_band+vwap_band.std()*0.5
    vwap_band_lower_1 = vwap_band-vwap_band.std()*0.5
    vwap_band_upper_2 = vwap_band+vwap_band.std()*1
    vwap_band_lower_2 = vwap_band-vwap_band.std()*1

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    #close_data = close_data.iloc[200:]

    # trend analysis
    trend = ema_trend(close_data, 50)
    hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
    list_of_lists = [len(hband), len(close_data), len(trend), len(vwap_band), len(vwap_band_lower_1)]
    #print(list_of_lists)
    min_list = min(list_of_lists)
    #print(min_list)
    close_data = close_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()


    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    #trend = pd.Series(trend).tail(min_list).to_list()
    vwap_band = vwap_band.tail(min_list).to_list()
    vwap_band_lower_1 = vwap_band_lower_1.tail(min_list).to_list()
    vwap_band_upper_1 = vwap_band_upper_1.tail(min_list).to_list()
    vwap_band_lower_2 = vwap_band_lower_2.tail(min_list).to_list()
    vwap_band_upper_2 = vwap_band_upper_2.tail(min_list).to_list()
    
    df = df[['open', 'high', 'low', 'close']]

    df = df.tail(min_list)
    #print(len(vwap_band))
    #print(vwap_band)
    #print(len(hband))
    #print(len(close_data))
    df['hband'] = hband
    df['lband'] = lband
    df['mband'] = mband
    df['vwap_band'] = vwap_band
    df['vwap_band_upper_1'] = vwap_band_upper_1
    df['vwap_band_lower_1'] = vwap_band_lower_1
    df['vwap_band_upper_2'] = vwap_band_upper_2
    df['vwap_band_lower_2'] = vwap_band_lower_2
    #print(df)
    #return df
    # if delta_vwap_band_upper_2
    
    #delta_lband = abs(lband[idx] - close_data[idx])
    #delta_hband = abs(hband[idx] - close_data[idx])
    #delta_mband = abs(mband[idx] - close_data[idx])
    #delta_vwap = abs(vwap_band[idx] - )
    #delta_vwap_band_lower_2 > low_data[idx] or abs(delta_vwap_band_lower_2-)

    #return df
    #print(df.head())

    results = []
    for idx in range(min_list):
        # Bollinger diff
        delta_lband = abs(lband[idx] - close_data[idx])
        delta_lband_low = abs(lband[idx] - low_data[idx])
        delta_hband = abs(hband[idx] - close_data[idx])
        delta_hband_high = abs(hband[idx] - high_data[idx])
        delta_mband = abs(mband[idx] - close_data[idx])
        # VWAP diff
        delta_vwap = abs(vwap_band[idx] - close_data[idx])
        delta_vwap_band_upper_1 = abs(vwap_band_upper_1[idx] - high_data[idx])
        delta_vwap_band_upper_2 = abs(vwap_band_upper_2[idx] - high_data[idx])
        delta_vwap_band_lower_1 = abs(vwap_band_lower_1[idx] - low_data[idx])
        delta_vwap_band_lower_2 = abs(vwap_band_lower_2[idx] - low_data[idx])
        
        if low_data[idx] < vwap_band_lower_2[idx] or delta_vwap_band_lower_2 < b_band_delta:
            if delta_lband_low < b_band_delta or low_data[idx] < lband[idx]:
                results.append('BUY')
            else:
                results.append('IGNORE')
        elif vwap_band_lower_2[idx] < low_data[idx] < vwap_band_lower_1[idx]:
            if delta_lband_low < b_band_delta or low_data[idx] < lband[idx]:
                results.append('BUY')
            else:
                results.append('IGNORE')

        elif high_data[idx] > vwap_band_upper_2[idx] or delta_vwap_band_upper_2 < b_band_delta:
            if delta_hband_high < b_band_delta or hband[idx] < high_data[idx]:
                results.append('SELL')
            else:
                results.append('IGNORE')

        elif vwap_band_upper_1[idx] < high_data[idx] < vwap_band_upper_2[idx]:
            if delta_hband_high < b_band_delta or hband[idx] < high_data[idx]:
                results.append('SELL')
            else:
                results.append('IGNORE')

        else:
            results.append('IGNORE')

    return results


# ---------------------------------------------------------------------------- #
#                              simple_bollinger_1                              #
# ---------------------------------------------------------------------------- #


def simple_bollinger_1(df, b_band_delta=0.0001):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    #close_data = close_data.iloc[200:]

    # trend analysis
    trend = ema_trend(close_data, 50)
    hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
    list_of_lists = [len(hband), len(close_data), len(trend)]
    #print(list_of_lists)
    min_list = min(list_of_lists)
    #print(min_list)
    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    trend = pd.Series(trend).tail(min_list).to_list()
    #print(len(close_data))
    #print(len(hband))
    
    #print(len(trend))
    results = []
    for i in range(min_list):
        delta_lband = abs(lband[i] - close_data[i])
        delta_hband = abs(hband[i] - close_data[i])
        delta_mband = abs(mband[i] - close_data[i])
        if trend[i] == 'UPTREND':
            if delta_lband < delta_mband and delta_lband < b_band_delta:
                results.append("BUY")
            elif delta_mband < delta_hband and delta_mband < b_band_delta:
                results.append("BUY")
            else:
                results.append("IGNORE")

        elif trend[i] == 'DOWNTREND':
            if delta_hband < delta_mband and delta_hband < b_band_delta or hband[i] < high_data[i]:
                results.append("SELL")
            elif delta_mband < delta_lband and delta_mband < b_band_delta and close_data[i] < mband[i]:
                results.append("SELL")
            else:
                results.append("IGNORE")

        elif trend[i] == 'SIDEWAYS':
            if delta_hband < delta_mband and delta_hband < b_band_delta or hband[i] < high_data[i]:
                results.append("SELL")
            elif delta_lband < delta_mband and delta_lband < b_band_delta or low_data[i] < lband[i]:
                results.append("BUY")
            else:
                results.append("IGNORE")
        else:
            results.append("IGNORE")

    return results


def simple_bollinger_2(df, b_band_delta=0.0001):

    close_data = df['close']
    low_data = df['low']
    high_data = df['high']
    #close_data = close_data.iloc[200:]

    # trend analysis
    trend = ema_trend(close_data, 50)
    hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
    list_of_lists = [len(hband), len(close_data), len(trend)]
    #print(list_of_lists)
    min_list = min(list_of_lists)
    #print(min_list)
    close_data = close_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    trend = pd.Series(trend).tail(min_list).to_list()
    #print(len(close_data))
    #print(len(hband))
    
    #print(len(trend))
    results = []
    for i in range(min_list):
        delta_lband_low = abs(lband[i] - low_data[i])
        delta_lband = abs(lband[i] - close_data[i])

        delta_hband = abs(hband[i] - close_data[i])
        delta_hband_high = abs(hband[i] - high_data[i])
        delta_mband = abs(mband[i] - close_data[i])

        if delta_lband < delta_mband and delta_lband_low < b_band_delta:# or close_data[i] < mband[i]:
            results.append("BUY")
        elif delta_hband < delta_mband and delta_hband_high < b_band_delta:# or mband[i] < close_data[i]:
                results.append("SELL")
        else:
            results.append("IGNORE")

    return results


""" timezone = pytz.timezone("EET")
tick = get_info_tick("AUDNZD")
#print(tick)
current_tick_time = datetime.fromtimestamp(tick.time, timezone).strftime("%Y-%m-%d %H:%M")
current_tick_time = datetime.strptime(current_tick_time, "%Y-%m-%d %H:%M")#+daylight_savings
#current_tick_time =  datetime.datetime.strftime(current_tick_time, "%Y-%m-%d %H:%M")
from_time = current_tick_time-timedelta(days=5)
historical_df = get_historical_trades("AUDNZD", from_time)
esl_time = historical_df.index.values[-1] """

#------------------------------------------------------------------------------
def actualtime():
    # datetime object containing current date and time
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    #print("date and time =", dt_string)
    return str(dt_string)
#------------------------------------------------------------------------------
def sync_60sec(op):

    info_time_new = datetime.strptime(str(actualtime()), '%d/%m/%Y %H:%M:%S')
    waiting_time = 60 - info_time_new.second

    t = Timer(waiting_time, op)
    t.start()

    print(actualtime(), f'waiting till next minute and 00 sec...')
#------------------------------------------------------------------------------
def program(symbol, tp, sl, lot):
    if not mt5.initialize(login=server_num, server=server_name, password=password):
        print("initialize() failed, error code =",mt5.last_error())

    timezone = pytz.timezone("EET")
    tick = get_info_tick(symbol)
    daylight_savings = timedelta(hours=3)
    current_tick_time = datetime.fromtimestamp(tick.time, timezone).strftime("%Y-%m-%d %H:%M")
    current_tick_time = datetime.strptime(current_tick_time, "%Y-%m-%d %H:%M")#+daylight_savings
    #current_tick_time =  datetime.datetime.strftime(current_tick_time, "%Y-%m-%d %H:%M")
    from_time = current_tick_time-timedelta(days=7)
    time_now = datetime.now(timezone)+daylight_savings
    ######### Change here the timeframe
    rates = mt5.copy_rates_range(symbol, mt5.TIMEFRAME_M1, from_time, time_now)

    rates_frame = pd.DataFrame(rates)
    rates_frame['time']=pd.to_datetime(rates_frame['time'], unit='s')
    print(rates_frame.tail(5))
    
    # If you want to work only with open, high, low, close you could use
    rates_frame = rates_frame.drop(['real_volume'], axis=1)

    print(f"\n", actualtime(),f"|| waiting for signals {symbol} ||\n")



    #------------------------------------------------------------------------------
    #                   E M E R G E N C Y   S T O P - L O S S 
    #------------------------------------------------------------------------------

    # set global variables
    global esl_bool
    global esl_time
    global esl_counter
    global esl_cooldown_bool
    historical_df = get_historical_trades(symbol, from_time)
    if historical_df is None:
        esl_bool = False
    else:
        esl_bool = check_esl(historical_df)
    #emergency_sl_bool = check_emergency_sl(historical_df)

    if esl_bool == True and esl_time != historical_df.index.values[-1] or esl_cooldown_bool == True and esl_bool==True:
        # activate emergency stop loss
        # ONLY ACTIVATES ONCE, NEED TO FIX THIS!!!
        print('EMERGENCY STOP LOSS ACTIVE')
        esl_counter+=1
        esl_cooldown_bool = True
        if esl_counter >= 19:
            esl_bool = False
            esl_time = historical_df.index.values[-1]
            esl_counter = -1
            esl_cooldown_bool = False
            print("EMERGENCY STOP LOSS INACTIVE, RESUMING TRADING MODE")
            pass
    else:
        esl_counter = -1
        esl_bool = False
        pass

    #------------------------------------------------------------------------------
    #                   A P P L Y   S T R A T E G Y 
    #------------------------------------------------------------------------------

    #signals = stochrsi_macd_bollinger_indicatorv2(rates_frame, b_band_delta=0.0001)
    #signals = check_macd_with_stochrsi(rates_frame)
    #signals = simple_bollinger_strategy_3(rates_frame)
    #signals = simple_bollinger_1(rates_frame)
    signals = simple_bollinger_2(rates_frame)
    #signals = vwap_bollinger_1(rates_frame)
    positions=mt5.positions_total()
    # check for any open positions, otherwise apply trading strategy

    #historical_df = get_historical_trades(symbol, from_time)
    #loss_counter = historical_df.tail(4)['profit'].sum()
    #if loss_counter <= -0.4:
    #    time.sleep(600)
    #else:
    print("CURRENT ESL_BOOL IS: %s" % esl_bool)
    deviation = 20  # Maximum number of pips to deviate from bid/ask price.
    if positions > 0:
        pass
    elif esl_counter < 0:
        print("SEARCHING FOR SIGNALS...")
        if len(signals) > 3:
            print("LAST 3 STRATEGY SIGNALS: %s" % signals[-3:])
        
        if signals[-1] == "BUY":
            result, buy_request = open_trade("BUY", symbol, lot, tp, sl, deviation)
            historical_df = get_historical_trades(symbol, from_time)
            #result = mt5.order_send(request)
            if historical_df is None:
                esl_time = ""
            else:
                esl_time = historical_df.index.values[-1]
            print('BUY SIGNAL ACTIVATED!')
            print(result)
        elif signals[-1] == "SELL":
            result, buy_request = open_trade("SELL", symbol, lot, tp, sl, deviation)
            historical_df = get_historical_trades(symbol, from_time)
            if historical_df is None:
                esl_time = ""
            else:
                esl_time = historical_df.index.values[-1]
            print('SELL SIGNAL ACTIVATED!')
            print(result)
        else:
            pass
    else:
        pass


# Im using AMPGlobalUSA-Demo Server
# starting mt5
       
#------------------------------------------------------------------------------
#                   S T A R T I N G   M T 5 
#------------------------------------------------------------------------------
#authorized=mt5.login(server_num, password=password)
#authorized=mt5.login(server_num)
authorized=mt5.login(account)  # the terminal database password is applied if connection data is set to be remembered
mt5.initialize(login=server_num, server=server_name, password=password)
tick = get_info_tick(symbol)

#print(tick)
if not mt5.initialize(login=server_num, server=server_name, password=password):
    print("initialize() failed, error code =",mt5.last_error())
    quit()   
if authorized:
    print("connected to account #{}".format(server_num))
else:
    print("failed to connect at account #{}, error code: {}".format(server_num, mt5.last_error()))
    

timezone = pytz.timezone("EET")
tick = get_info_tick(symbol)
daylight_savings = timedelta(hours=3)
current_tick_time = datetime.fromtimestamp(tick.time, timezone).strftime("%Y-%m-%d %H:%M")
current_tick_time = datetime.strptime(current_tick_time, "%Y-%m-%d %H:%M")#+daylight_savings
#current_tick_time =  datetime.datetime.strftime(current_tick_time, "%Y-%m-%d %H:%M")
from_time = current_tick_time-timedelta(days=5)
historical_df = get_historical_trades(symbol, from_time)
if historical_df is None:
    esl_time = ""
else:
    esl_time = historical_df.index.values[-1]

if authorized:
    account_info=mt5.account_info()
    if account_info!=None:       
        account_info_dict = mt5.account_info()._asdict()
        df=pd.DataFrame(list(account_info_dict.items()),columns=['property','value'])
        print("account_info() as dataframe:")
        print(df)
else:
    print(f"failed to connect to trade account {server_num} with password={password}, error code =",mt5.last_error())

#------------------------------------------------------------------------------
def trading_bot():
    symbol  = "EURUSD"
    tp = 5
    sl = 2
    # trade volume in lots, calculated by account balance and risk
    #risk = 0.05
    while True:
        try:
            #program(symbol_1)
            #account_info=mt5.account_info()._asdict()
            #margin = account_info['balance']*risk
            #lot = (margin*account_info['leverage'])/100000
            #lot = round(lot, 2)
            #print('Current lot size: %s' % lot)

            program(symbol, tp, sl, 0.01)
            time.sleep(59.8) # it depends on your computer and ping
        except KeyboardInterrupt:
            break

sync_60sec(trading_bot)

# ---------------------------------------------------------------------------- #
#                                     TO DO                                    #
# ---------------------------------------------------------------------------- #

# FIX EMERGENCY STOP LOSS FOR UNIQUE TRADES

# starting balance = 1728.69

# possible tp/sl values = 6/2, 5/3, 
