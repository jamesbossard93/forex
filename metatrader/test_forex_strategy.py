import MetaTrader5 as mt5
import time
import pandas as pd
from pandas.core.indexing import convert_from_missing_indexer_tuple
import pytz
from datetime import datetime
import datetime as dt
import numpy as np
from ta import add_all_ta_features
from ta import trend
from ta.utils import dropna
from ta.volatility import BollingerBands
from ta.momentum import stochrsi, stochrsi_d
from ta.momentum import StochRSIIndicator
from ta.trend import sma_indicator
from ta.trend import ema_indicator
from ta.trend import macd_diff
from ta.trend import PSARIndicator
from ta.volatility import average_true_range
from ta.volume import volume_weighted_average_price
from datetime import timedelta
from ta.volume import chaikin_money_flow
from ta.volume import money_flow_index
import math

pd.set_option('display.max_columns', 500) # number of columns to be displayed
pd.set_option('display.width', 1500)      # max table width to display
import plotly.graph_objs as go

pd.set_option('display.max_columns', 10) # number of columns to be displayed
pd.set_option('display.max_rows', 100)
pd.set_option('display.width', 1500)      # max table width to display


# connect to the trade account without specifying a password and a server
server_num = "Add account number here"
server_name = "Add account name here"

password = "Add account password here"
authorized=mt5.login(server_num)  # the terminal database password is applied if connection data is set to be remembered

if not mt5.initialize("C:\\Program Files\\MetaTrader 5-1\\terminal64.exe", login=server_num, server=server_name, password=password):
    print("initialize() failed, error code =",mt5.last_error())
    quit()

ea_magic_number = 9987989 # if you want to give every bot a unique identifier
print("authorized: %s" % authorized)
# login
if authorized:
    print("connected to account #{}".format(server_num))
else:
    print("failed to connect at account #{}, error code: {}".format(server_num, mt5.last_error()))



def get_data(symbol, from_time, time_frame):
    # if returns null and timeframe (tf) is M1, its usually because of the 100,000 bar limit.
    # Uses GMT+3 time zone as FBS trader uses GMT+3 for metatrader 5
    #end=datetime.datetime.now(pytz.timezone("Etc/GMT+3"))
    mt5_timezone = pytz.timezone("EET")
    end=dt.datetime.now(mt5_timezone)
    #print("to_time: %s" % end)
    #mt5.COPY_TICKS_TRADE
    copy_rates = mt5.copy_rates_range(symbol, time_frame ,from_time, end)
    df_rates = pd.DataFrame(copy_rates)
    df_time = df_rates['time'].to_numpy()
    dftime = []
    for i in range(len(df_time)):
        dftime.append(datetime.fromtimestamp(df_time[i], mt5_timezone).strftime("%Y-%m-%d %H:%M"))
    x = pd.Series(dftime)
    df_rates['time'] = x
    df_rates.set_index('time', drop=True, inplace=True)
    df_rates["symbol"]=symbol
    return df_rates






def ema_trendv2(close_data, window):

    ema = ema_indicator(close_data, window=window, fillna=False).dropna()

    results = []

    for i in range(2, len(ema)):
        if ema.iloc[i-2] < ema.iloc[i-1] < ema.iloc[i]:
            results.append("UPTREND")
        elif ema.iloc[i-2] > ema.iloc[i-1] > ema.iloc[i]:
            results.append("DOWNTREND")
        else:
            results.append("SIDEWAYS")
    
    return results


def describe_results(results):
    # Returns a dictionary of the details of the results
    wl_ratio = 0
    wins = 0
    loss = 0
    average_loss = []
    average_win = []
    max_consecutive_wins = []
    max_consecutive_loss = []
    #print(results)
    for i in results['PL']:
        #print(i)
        if i > 0:
            wins+=1
            average_win.append(i)
        else:
            loss+=1
            average_loss.append(i)
    #print(average_loss)
    if len(average_loss) < 1:
        profit_factor = abs(sum(average_win)/1)
        average_loss = 0
        max_loss = 0
    else:
        profit_factor = abs(sum(average_win)/sum(average_loss))
        max_loss = min(average_loss)
        average_loss = sum(average_loss)/loss
        

    max_win = max(average_win)
    average_win = sum(average_win)/wins
    total_trades = wins+loss
    win_ratio = wins/(wins+loss)
    loss_ratio = loss/(wins+loss)
    results_details = {'win_rate': win_ratio, 'loss_rate': loss_ratio,
        'average_win': average_win, 'average_loss': average_loss, 
        'total_wins': wins, 'total_loss': loss, 'total_trades': total_trades, 
        'profit_factor': profit_factor, 'total_PL': results['PL'].sum(), 
        'max_win': max_win, 'max_loss': max_loss}
    results_details = dict(results_details)

    #for key, value in results_details.items():
    #    print("%s: %s" % (key, value))



    return results_details

def calculate_profitability(tp, sl, description, win_error=0.025):
    # tp = take profit in pips
    # sl = stop loss in pips
    # description = dictionary of wins/loss results
    # win_error = max error allowed in win rate to account for real
    # results. must be in decimal.

    win_rate_required = sl/(sl+tp)

    pred_win_rate = description['win_rate']

    if (win_rate_required+win_error) < pred_win_rate:
        print("PROFTIABLE GIVEN THE EXPECTED WIN RATE AND TP/SL SETTINGS!!")
        print("WIN RATE REQUIRED: %s, WIN ERROR ALLOWED: %s" % (win_rate_required, win_error))
        print("EXPECTED WIN RATE: %s" % pred_win_rate)
        print("WIN RATE DIFFERENCE: %s" % (pred_win_rate-win_rate_required))
    else:
        print("NOT PROFITABLE WITH THE EXPECTED WIN RATE AND TP/SL SETTINGS!!")
        print("WIN RATE REQUIRED: %s" % win_rate_required)
        print("EXPECTED WIN RATE: %s" % pred_win_rate)
        print("WIN RATE DIFFERENCE: %s" % (pred_win_rate-win_rate_required))

def ema_trend(df, window):
    close_data = df['close']
    ema = ema_indicator(close_data, window=window, fillna=False).dropna()

    results = []

    for i in range(2, len(ema)):
        if ema.iloc[i-2] < ema.iloc[i-1] < ema.iloc[i]:
            results.append("UPTREND")
        elif ema.iloc[i-2] > ema.iloc[i-1] > ema.iloc[i]:
            results.append("DOWNTREND")
        else:
            results.append("SIDEWAYS")
    
    return results




# ---------------------------------------------------------------------------- #
#                        SIMPLE BOLLINGER BANDS STRATEGY                       #
# ---------------------------------------------------------------------------- #

def simple_bollinger_strategy_1(df, bb_delta=0.0001):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    trend = ema_trend(close_data, 200)

    hband = BollingerBands(df['close'], 50, 3, False).bollinger_hband().dropna()
    lband = BollingerBands(df['close'], 50, 3, False).bollinger_lband().dropna()
    mband = BollingerBands(df['close'], 50, 3, False).bollinger_mavg().dropna()

    psar_d = PSARIndicator(df['high'], df['low'], df['close']).psar_down().dropna()
    psar_u = PSARIndicator(df['high'], df['low'], df['close']).psar_up().dropna()
    min_list = min([len(hband), len(psar_d), len(psar_u), len(close_data)])

    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()

    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()


    results = []

    for idx in range(min_list):

        delta_lband = abs(lband[idx] - close_data[idx])
        delta_hband = abs(hband[idx] - close_data[idx])
        delta_mband = abs(mband[idx] - close_data[idx])

        delta_high_hband = high_data[idx] - hband[idx]
        delta_low_lband = low_data[idx] - lband[idx]

        if delta_hband < delta_mband and delta_high_hband >= bb_delta:
            results.append("SELL")

        elif delta_lband < delta_mband and delta_low_lband <= bb_delta:
            results.append("BUY")

        else:
            results.append("IGNORE")

    return results



# ---------------------------------------------------------------------------- #
#                              simple_bollinger_1                              #
# ---------------------------------------------------------------------------- #


def simple_bollinger_1(df, b_band_delta=0.0001):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    #close_data = close_data.iloc[200:]

    # trend analysis
    trend = ema_trend(df, 50)
    hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
    list_of_lists = [len(hband), len(close_data), len(trend)]
    #print(list_of_lists)
    min_list = min(list_of_lists)
    #print(min_list)
    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    trend = pd.Series(trend).tail(min_list).to_list()
    #print(len(close_data))
    #print(len(hband))
    
    #print(len(trend))
    results = []
    for i in range(min_list):
        delta_lband = abs(lband[i] - close_data[i])
        delta_hband = abs(hband[i] - close_data[i])
        delta_mband = abs(mband[i] - close_data[i])
        if trend[i] == 'UPTREND':
            if delta_lband < delta_mband and delta_lband < b_band_delta:
                results.append("BUY")
            elif delta_mband < delta_hband and delta_mband < b_band_delta:
                results.append("BUY")
            else:
                results.append("IGNORE")

        elif trend[i] == 'DOWNTREND':
            if delta_hband < delta_mband and delta_hband < b_band_delta or hband[i] < high_data[i]:
                results.append("SELL")
            elif delta_mband < delta_lband and delta_mband < b_band_delta and close_data[i] < mband[i]:
                results.append("SELL")
            else:
                results.append("IGNORE")

        elif trend[i] == 'SIDEWAYS':
            if delta_hband < delta_mband and delta_hband < b_band_delta or hband[i] < high_data[i]:
                results.append("SELL")
            elif delta_lband < delta_mband and delta_lband < b_band_delta or low_data[i] < lband[i]:
                results.append("BUY")
            else:
                results.append("IGNORE")
        else:
            results.append("IGNORE")

    return results


# ---------------------------------------------------------------------------- #
#     TRIPLE INDICATOR STRATEGY: STOCH_RSI+MACD_HISTOGRAM+BOLLINGER_BANDS V2   #
# ---------------------------------------------------------------------------- #

def stochrsi_macd_bollinger_indicatorv2(df, b_band_delta):
    # THIS STRATEGY IS A CONTINUATION TREND STRATEGY.
    # IT DOES NOT TRY TO 'CHASE' THE TOPS AND BOTTOMS LIKE
    # MOST STRATEGIES.

    close_data = df['close']
    #close_data = close_data.iloc[200:]

    # trend analysis
    #trend = ema_ribbon_trend(close_data)
    #trend = ema_trend(close_data, 10)
    trend = ema_trendv2(close_data, 50)
    macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()
    hband = BollingerBands(close_data, 20, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 20, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 20, 3, False).bollinger_mavg()
    min_list = min([len(stoch_rsi_d), len(hband), len(macd_histogram), len(close_data), len(trend)])

    df_times = df.index.to_numpy()
    df_times = pd.Series(df_times).tail(min_list).to_list()

    close_data = close_data.tail(min_list).to_list()
    macd_histogram = macd_histogram.tail(min_list).to_list()
    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    # this gives better results for some reason?
    #trend = trend[:min_list]
    trend = pd.Series(trend).tail(min_list).to_list()
    #trend = trend[-min_list:]
    # ---------------------------------------------------------------------------- #
    #                                     LISTS                                    #
    # ---------------------------------------------------------------------------- #

    series_list = [len(trend), len(lband), len(hband), len(mband), len(stoch_rsi_d),
        len(macd_histogram), len(close_data), len(df_times)]

    lband_len = len(lband) - min(series_list)
    trend_len = len(trend) - min(series_list)
    macd_len = len(macd_histogram) - min(series_list)
    stoch_len = len(stoch_rsi_d) - min(series_list)
    close_len = len(close_data) - min(series_list)
    times_len = len(df_times) - min(series_list)
    if lband_len > 0:
        hband = hband[lband_len:]
        lband = lband[lband_len:]
        mband = mband[lband_len:]

    if trend_len > 0:
        trend = trend[trend_len:]
    if macd_len > 0:
        macd_histogram = macd_histogram[macd_len:]
    if close_len > 0:
        close_data = close_data[close_len:]
    if stoch_len > 0:
        stoch_rsi_d = stoch_rsi_d[stoch_len:]
    if times_len > 0:
        df_times = df_times[times_len:]

    test_list = []
    results_list = []

    #from_time = current_tick_time-timedelta(hours=3)
    #time_now = datetime.now(timezone)+daylight_savings
    ######### Change here the timeframe
    #hourly_df = mt5.copy_rates_range(symbol, mt5.TIMEFRAME_H1, from_time, time_now)

    for i in range(min_list):
        #current_tick_time = df_times[i]
        #from_time = current_tick_time-timedelta(hours=3)
        #trend =
        #from_time = current_tick_time-timedelta(days=2)
        #time_now = datetime.now(timezone)+daylight_savings

        # first do something
        delta_lband = abs(lband[i] - close_data[i])
        delta_hband = abs(hband[i] - close_data[i])
        delta_mband = abs(mband[i] - close_data[i])
        # 
        #print("delta_lband: %s" % delta_lband)
        #print("delta_hband: %s" % delta_hband)
        #print("delta_mband: %s" % delta_mband)
        # FIX THE DELTAS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # check ema trend, if uptrend-> look for buys etc..
        if trend[i] == "UPTREND":
            # check for pullback during uptrend
            if delta_mband <= b_band_delta:
                if 0 < macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5:
                    # pullback BUY
                    results_list.append("BUY")
                    test_list.append(("LONG", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    test_list.append(("IGNORE", df_times[i], close_data[i]))
                    continue
            elif lband[i] < close_data[i] < mband[i]:
                if macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5: 
                    # pullback BUY
                    results_list.append("BUY")
                    test_list.append(("BUY", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    test_list.append(("IGNORE", df_times[i], close_data[i]))
                    continue
            else:
                results_list.append("IGNORE")
                continue

        elif trend[i] == "DOWNTREND":
            # check for potential sells during pullback
            asdf = delta_mband
            if delta_mband <= b_band_delta:
                if -0.2 < macd_histogram[i] < 0 and 0.3 <= stoch_rsi_d[i]:
                    # pullback SELL
                    results_list.append("SELL")
                    test_list.append(("SHORT", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    test_list.append(("IGNORE", df_times[i], close_data[i]))
                    continue                    
            else:
                results_list.append("IGNORE")
                continue
        else:
            # SIDEWAYS MARKET
            # check macd and stochrsi here!
            
            if delta_hband <= b_band_delta:
                # check macd and stochrsi for sell
                if macd_histogram[i] >= 0 and stoch_rsi_d[i] > 0.5:
                    results_list.append("SELL")
                    test_list.append(("SHORT", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    continue
            elif delta_lband <= b_band_delta:
                if macd_histogram[i] <= 0 and stoch_rsi_d[i] < 0.5:
                    results_list.append("BUY")
                    test_list.append(("LONG", df_times[i], close_data[i]))
                    continue
                else:
                    results_list.append("IGNORE")
                    continue
    # returns a list with the oldest data first.
    return results_list


# ---------------------------------------------------------------------------- #
#       TRIPLE THREAT STRATEGY: STOCH_RSI+MACD_HISTOGRAM+BOLLINGER_BANDS V2    #
# ---------------------------------------------------------------------------- #

def stochrsi_macd_bollinger_indicator(df, b_band_delta):
    # THIS STRATEGY IS A CONTINUATION TREND STRATEGY.
    # IT DOES NOT TRY TO 'CHASE' THE TOPS AND BOTTOMS LIKE
    # MOST STRATEGIES.

    close_data = df['close']
    #close_data = close_data.iloc[200:]

    # trend analysis
    #trend = ema_ribbon_trend(close_data)
    trend = ema_trend(close_data, 50)

    macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()
    hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
    min_list = min([len(stoch_rsi_d), len(hband), len(macd_histogram), len(close_data), len(trend)])

    df_times = df.index.to_numpy()
    df_times = pd.Series(df_times).tail(min_list).to_list()

    close_data = close_data.tail(min_list).to_list()
    macd_histogram = macd_histogram.tail(min_list).to_list()
    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    trend = pd.Series(trend).tail(min_list).to_list()
    results = []
    for i in range(min_list):
        delta_lband = abs(lband[i] - close_data[i])
        delta_hband = abs(hband[i] - close_data[i])
        delta_mband = abs(mband[i] - close_data[i])

        if trend[i] == "UPTREND":
            # check for pullback during uptrend
            if delta_mband <= b_band_delta:
                if 0 < macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5:
                    # pullback BUY
                    results.append("BUY")
                    continue
                else:
                    results.append("IGNORE")
            elif lband[i] < close_data[i] < mband[i]:
                if macd_histogram[i] < 0.2 and stoch_rsi_d[i] < 0.5: 
                    # pullback BUY
                    results.append("BUY")
                else:
                    results.append("IGNORE")
            else:
                results.append("IGNORE")
                continue

        elif trend[i] == "DOWNTREND":
            # check for potential sells during pullback
            asdf = delta_mband
            if delta_mband <= b_band_delta:
                if -0.2 < macd_histogram[i] < 0 and 0.3 <= stoch_rsi_d[i]:
                    # pullback SELL
                    results.append("SELL")
                    continue
                else:
                    results.append("IGNORE")
                    continue                    
            else:
                results.append("IGNORE")
                continue
        else:
            # SIDEWAYS MARKET
            # check macd and stochrsi here!
            
            if delta_hband <= b_band_delta:
                # check macd and stochrsi for sell
                if macd_histogram[i] >= 0 and stoch_rsi_d[i] > 0.5:
                    results.append("SELL")
                    continue
                else:
                    results.append("IGNORE")
                    continue
            elif delta_lband <= b_band_delta:
                if macd_histogram[i] <= 0 and stoch_rsi_d[i] < 0.5:
                    results.append("BUY")
                    continue
                else:
                    results.append("IGNORE")

    return results


def check_hourly_conf(df):
    # NEED TO FIX THIS
    conf_close = df['close']
    conf_ema8 = ema_indicator(conf_close, window=8, fillna=False).dropna()
    
    uptrend_bool_8 = conf_ema8.iloc[-2] < conf_ema8.iloc[-1] 
    downtrend_bool_8 = conf_ema8.iloc[-2] > conf_ema8.iloc[-1] 

    if uptrend_bool_8 == True:
        return "UPTREND", conf_ema8.tail(1).values[0]

    elif downtrend_bool_8 == True:
        return "DOWNTREND", conf_ema8.tail(1).values[0]
    else:
        return "SIDEWAYS", conf_ema8.tail(1).values[0]



def check_five_min(df):

    conf_close = df['close']

    conf_ema8 = ema_indicator(conf_close, window=8, fillna=False)

    conf_ema13 = ema_indicator(conf_close, window=13, fillna=False)

    conf_ema21 = ema_indicator(conf_close, window=21, fillna=False)

    conf_close = conf_close.tail(len(conf_ema21))
    conf_ema13 = conf_ema13.tail(len(conf_ema21))
    conf_ema8 = conf_ema8.tail(len(conf_ema21))

    results = []

    for i in range(2, len(conf_ema21)):
        uptrend_bool_8 = conf_ema8.iloc[i-2] < conf_ema8.iloc[i-1] < conf_ema8.iloc[i]
        uptrend_bool_13 = conf_ema13.iloc[i-2] < conf_ema13.iloc[i-1] < conf_ema13.iloc[i]
        uptrend_bool_21 = conf_ema21.iloc[i-2] < conf_ema21.iloc[i-1] < conf_ema21.iloc[i]

        downtrend_bool_8 = conf_ema8.iloc[i-2] > conf_ema8.iloc[i-1] > conf_ema8.iloc[i]
        downtrend_bool_13 = conf_ema13.iloc[i-2] > conf_ema13.iloc[i-1] > conf_ema13.iloc[i]
        downtrend_bool_21 = conf_ema21.iloc[i-2] > conf_ema21.iloc[i-1] > conf_ema21.iloc[i]

        if (downtrend_bool_8 == True) and (downtrend_bool_21 == True) and (downtrend_bool_13 == True):
            if conf_close.iloc[i] < conf_ema8.iloc[i] < conf_ema21.iloc[i]:
                results.append("DOWNTREND")
            else:
                results.append("SIDEWAYS")

        elif (uptrend_bool_8 == True) and (uptrend_bool_21 == True) and (uptrend_bool_13 == True):
            if conf_ema21.iloc[i] < conf_ema8.iloc[i] < conf_close.iloc[i]:
                results.append("UPTREND")
            else:
                results.append("SIDEWAYS")
        else:
            results.append("SIDEWAYS")

    return results


# DO PULLBACK

# ---------------------------------------------------------------------------- #
#                          SUPERTREND HELPER FUNCTIONS                         #
# ---------------------------------------------------------------------------- #

def hl2(df):
    ''' Returns a list of (high+low)/2'''

    high = df['high']

    low = df['low']

    hl2_list = []

    for i in range(len(df)):
        
        tmp = (high[i]+low[i])/2
        #tmp = round(tmp, 5)
        hl2_list.append(tmp)

    return hl2_list

def ohlc4(df):
    ''' Returns a list of (open+high+low+close)/4'''
    open = df['open']
    high = df['high']
    low = df['low']
    close = df['close']
    ohlc = []

    for i in range(len(df)):

        tmp = (open[i]+high[i]+low[i]+close[i])/4

        ohlc.append(tmp)

    return ohlc


def true_range(df):

    high = df['high']
    low = df['low']
    close = df['close']

    tr = []

    for i in range(1, len(df)):

        tr1 = high[i]-low[i]
        tr2 = abs(high[i] - close[i-1])
        tr3 = abs(low[i] - close[i-1])

        tr.append(max([tr1, tr2, tr3]))

    return tr

# ---------------------------------------------------------------------------- #
#                                ATR SUPERTREND                                #
# ---------------------------------------------------------------------------- #

def atr_trend(df, win, multiplier):
    
    close = df['close']


    src = hl2(df)

    atr = average_true_range(df['high'], df['low'], df['close'], window=win, fillna=False).dropna()
    uplist = []
    downlist = []
    trendlist = []
    past_trend = 1
    signals = []
    current_signal = "IGNORE"
    # NEED TO ADD BUY/SELL SIGNAL!
    # buySignal = trend == 1 and trend[1] == -1
    # sellSignal = trend == -1 and trend[1] == 1
    for i in range(1, len(atr)):

        up = src[i] - multiplier * atr[i]
        up_1 = src[i-1] - multiplier * atr[i-1]
        if close[i-1] > up_1:
            uplist.append(max([up,up_1]))
        else:
            uplist.append(up)
        down = src[i] + multiplier * atr[i]
        down_1 = src[i-1] + multiplier * atr[i-1]
        if close[i-1] < down_1:
            downlist.append(min([down,down_1]))
        else:
            downlist.append(down)

        if close[i] > down_1:
            trendlist.append(1)
            if past_trend == -1:
                past_trend = 1
                #current_signal = "SELL"
                signals.append("BUY")
                
        elif close[i] < up_1:
            trendlist.append(-1)
            if past_trend == 1:
                past_trend = -1
                #current_signal = "BUY"
                signals.append("SELL")

        else:
            signals.append("IGNORE")
            trendlist.append(0)

    return signals

# ---------------------------------------------------------------------------- #
#                               ATR SUPERTREND V2                              #
# ---------------------------------------------------------------------------- #


def atr_trendv2(df, win, multiplier):
    
    close = df['close']

    trend = ema_trendv2(close, 20)
    src = hl2(df)

    atr = average_true_range(df['high'], df['low'], df['close'], window=win, fillna=False).dropna().to_list()
    uplist = []
    downlist = []
    trendlist = []
    past_trend = 1
    signals = []
    current_signal = "IGNORE"
    # NEED TO MAKE SURE ALL LISTS MATCH!!!!!
    len_list = [len(atr), len(trend), len(src), len(close)]

    atr_len = len(atr) - min(len_list)
    trend_len = len(trend) - min(len_list)
    src_len = len(src) - min(len_list)
    close_len = len(close) - min(len_list)
    if atr_len > 0:
        atr = atr[atr_len:]
    if trend_len > 0:
        trend = trend[trend_len:]
    if src_len > 0:
        src = src[src_len:]
    if close_len > 0:
        close = close[close_len:]
    
    # NEED TO ADD BUY/SELL SIGNAL!
    # buySignal = trend == 1 and trend[1] == -1
    # sellSignal = trend == -1 and trend[1] == 1
    for i in range(1, min(len_list)):

        up = src[i] - multiplier * atr[i]
        up_1 = src[i-1] - multiplier * atr[i-1]
        if close[i-1] > up_1:
            uplist.append(max([up,up_1]))
        else:
            uplist.append(up)
        down = src[i] + multiplier * atr[i]
        down_1 = src[i-1] + multiplier * atr[i-1]
        if close[i-1] < down_1:
            downlist.append(min([down,down_1]))
        else:
            downlist.append(down)

        if close[i] > down_1:
            trendlist.append(1)
            if past_trend == -1:
                past_trend = 1
                #current_signal = "SELL"
                if trend[i] == "UPTREND":
                    signals.append("BUY")
                else:
                    signals.append("IGNORE")
        elif close[i] < up_1:
            trendlist.append(-1)
            if past_trend == 1:
                past_trend = -1
                #current_signal = "BUY"
                if trend[i] == "DOWNTREND":
                    signals.append("SELL")
                else:
                    signals.append("IGNORE")
                #signals.append("SELL")

        else:
            signals.append("IGNORE")
            trendlist.append(0)

    return signals




# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 1                               #
# ---------------------------------------------------------------------------- #

def forex_strategy_1(df):
    close_data = df['close']
    trend = ema_trend(close_data, 50)
    results = []
    for i in range(len(trend)):
        if trend[i] == 'UPTREND':
            results.append("BUY")
        elif trend[i] == 'DOWNTREND':
            results.append("SELL")
        else:
            results.append("IGNORE")

    return results


# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 3                               #
# ---------------------------------------------------------------------------- #

def forex_strategy_3(df):
    # if conf_hour == "DOWNTREND" && CMF<= 0 (sell mode initiated, look for potential entry point)
    # OR if 
    # then SELL

    # if conf_hour == "UPTREND" && CMF>= 0 (buy mode initiated, look for potential entry point)
    #     if last_close ~ ema_60
    #         buy
    #     elif last_Close < ema_60 OR last_close <= bband_delta
    #         buy
    #
    df_close = df['close']
    high = df['high']
    low = df['low']
    close = df['close']
    volume = df['tick_volume']
    ema_trend = ema_indicator(close, window=200, fillna=False).dropna()
    hband = BollingerBands(close, 20, 3, False).bollinger_hband().tail(len(ema_trend))
    lband = BollingerBands(close, 20, 3, False).bollinger_lband().tail(len(ema_trend))
    mband = BollingerBands(close, 20, 3, False).bollinger_mavg().tail(len(ema_trend))
    #close = close.tail(len(hband))
    results = []
    df_times = df.index.to_numpy()
    df_times = pd.Series(df_times).tail(len(ema_trend)).to_list()

    CMF = chaikin_money_flow(high, low, close, volume, window=50, fillna=False).tail(len(ema_trend))

    df_close = close.tail(len(ema_trend))

    # make list of lengths
    # find minimum


    for idx in range(1, len(ema_trend)):
        delta_lband = abs(lband.iloc[idx] - df_close.iloc[idx])
        delta_hband = abs(hband.iloc[idx] - df_close.iloc[idx])
        delta_mband = abs(mband.iloc[idx] - df_close.iloc[idx])

        if ema_trend[idx-1] < ema_trend[idx]:

            if CMF[idx] >= 0.05 and CMF[idx-1] < CMF[idx]:
                # buy mode initiated, searching for entry point...
                if delta_lband < delta_mband and df_close.iloc[idx] < ema_trend[idx]:
                    # if the bar is closer to lower band than middle.
                    results.append("BUY")
                    #print("BUY")
                elif delta_mband < delta_lband:
                    # if the bar is closer to the middle band...
                    results.append("BUY")
                    #print("BUY")                
                else:
                    results.append("IGNORE")
        
        elif ema_trend[idx] < ema_trend[idx-1]:
            if CMF[idx] <= -0.05 and CMF[idx-1] > CMF[idx]:
                if delta_hband < delta_mband:
                    # if the bar is closer to the upper band...
                    results.append("SELL")
                    #print("SELL")
                elif delta_lband < delta_mband:
                    results.append("BUY")
                else:
                    results.append("IGNORE")
        elif ema_trend[idx-1] < ema_trend[idx] and CMF[idx] < -0.05:
            if delta_hband < delta_mband:
                results.append("SELL")
                #print("SELL")
            # sell mode initiated, searching for entry point...
            else:
                results.append("IGNORE")
        else:
            results.append("IGNORE")
    return results


# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 4                               #
# ---------------------------------------------------------------------------- #


def forex_strategy_4(df):
    # stuff
    close = df['close']

    conf_ema = ema_indicator(close, window=21, fillna=False).dropna()
    close_ema = ema_indicator(close, window=8, fillna=False).dropna().tail(len(conf_ema))
    #ema_13 = ema_indicator(close, window=13, fillna=False).dropna().tail(len(conf_ema))
    results = []
    for idx in range(2, len(conf_ema)):

        conf_bool_up = conf_ema[idx-1] < conf_ema[idx] 
        conf_bool_down = conf_ema[idx-1] > conf_ema[idx]

        #ema_13_bool_up = ema_13[idx-2] < ema_13[idx-1] < ema_13[idx]
        #ema_13_bool_down = ema_13[idx-2] > ema_13[idx-1] > ema_13[idx]

        close_bool_up = close_ema[idx-2] < close_ema[idx-1] < close_ema[idx]
        close_bool_down = close_ema[idx] < close_ema[idx-1] < close_ema[idx-2]

        if conf_bool_up and close_bool_up:
            results.append("BUY")

        elif conf_bool_down and close_bool_down:
            results.append("SELL")
        else:
            results.append("IGNORE")

    return results


# ---------------------------------------------------------------------------- #
#                         FOREX STRATEGY 5: SUPERTREND                         #
# ---------------------------------------------------------------------------- #

def forex_strategy_5(df):
    # stuff
    close = df['close']
    conf_ema = ema_indicator(close, window=200, fillna=False).dropna()

    close_ema = ema_indicator(close, window=8, fillna=False).dropna()
    mfi = money_flow_index(df['high'], df['low'], close, df['tick_volume'], window=20).dropna()
    supertrend =  atr_trend(df, 10, 3)
    supertrend = pd.Series(supertrend).dropna()
    results = []

    min_list = min([len(mfi), len(close_ema), len(supertrend), len(close)])

    close = close.tail(min_list).to_list()
    mfi = mfi.tail(min_list).to_list()
    close_ema = close_ema.tail(min_list).to_list()
    supertrend = supertrend.tail(min_list).to_list()

    #print(supertrend[-5:])
    # NEED TO FIX SUPERTREND!!!!!!!!!!!!!!!!!!!!!!!!!


    for idx in range(min_list):


        if close[idx] > conf_ema[idx]:
            # overbought?
            #print("mfi: %s" % mfi[idx])
            if mfi[idx] > 50 and supertrend[idx] == "SELL":
                results.append("SELL")            

            else:
                results.append("IGNORE")

        elif conf_ema[idx] < close[idx]:
            #print("mfi: %s" % mfi[idx])
            if mfi[idx] < 50 and supertrend[idx] == "BUY":
                results.append("BUY")
            
            else:
                results.append("IGNORE")
        
        else:
            results.append("IGNORE")

    return results


# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 6                               #
# ---------------------------------------------------------------------------- #

def forex_strategy_6(df):
    # stuff
    close = df['close']

    conf_ema = ema_indicator(close, window=200, fillna=False).dropna()
    close_ema = ema_indicator(close, window=8, fillna=False).dropna().tail(len(conf_ema))

    close_ema = close_ema.to_list()
    conf_ema = conf_ema.to_list()


    results = []
    for idx in range(2, len(conf_ema)):

        conf_bool_up = conf_ema[idx-1] < conf_ema[idx] 
        conf_bool_down = conf_ema[idx-1] > conf_ema[idx]

        close_bool_up = close_ema[idx-2] < close_ema[idx-1] < close_ema[idx]
        close_bool_down = close_ema[idx] < close_ema[idx-1] < close_ema[idx-2]

        if close_ema[idx] > conf_ema[idx]:
            if close_bool_up and conf_bool_up:
                results.append("BUY")
            else:
                results.append("IGNORE")

        elif close_ema[idx] < conf_ema[idx]:
            if close_bool_down and conf_bool_down:
                results.append("SELL")
            else:
                results.append("IGNORE")
        else:
            results.append("IGNORE")

    return results

# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 7                               #
# ---------------------------------------------------------------------------- #


def forex_strategy_7(df):
    # stuff
    close = df['close']

    conf_ema = ema_indicator(close, window=21, fillna=False).dropna()
    close_ema = ema_indicator(close, window=8, fillna=False).dropna().tail(len(conf_ema))
    ema_13 = ema_indicator(close, window=13, fillna=False).dropna().tail(len(conf_ema))
    results = []
    for idx in range(2, len(conf_ema)):

        conf_bool_up = conf_ema[idx-1] < conf_ema[idx] 
        conf_bool_down = conf_ema[idx-1] > conf_ema[idx]

        ema_13_bool_up = ema_13[idx-2] < ema_13[idx-1] < ema_13[idx]
        ema_13_bool_down = ema_13[idx-2] > ema_13[idx-1] > ema_13[idx]

        close_bool_up = close_ema[idx-2] < close_ema[idx-1] < close_ema[idx]
        close_bool_down = close_ema[idx] < close_ema[idx-1] < close_ema[idx-2]

        if conf_bool_up and close_bool_up and ema_13_bool_up:
            results.append("BUY")

        elif conf_bool_down and close_bool_down and ema_13_bool_down:
            results.append("SELL")
        else:
            results.append("IGNORE")

    return results


# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 8                               #
# ---------------------------------------------------------------------------- #

def forex_strategy_8(df):


    psar_d = PSARIndicator(df['high'], df['low'], df['close']).psar_down_indicator().to_list()
    psar_u = PSARIndicator(df['high'], df['low'], df['close']).psar_up_indicator().to_list()
    hband = BollingerBands(df['close'], 50, 3, False).bollinger_hband().dropna()
    lband = BollingerBands(df['close'], 50, 3, False).bollinger_lband().dropna()
    mband = BollingerBands(df['close'], 50, 3, False).bollinger_mavg().dropna()

    #psar_d = PSARIndicator(df['high'], df['low'], df['close']).psar_down().to_list()#.dropna().to_list()
    #psar_u = PSARIndicator(df['high'], df['low'], df['close']).psar_up().to_list()#.dropna().to_list()
    results = []


    for i in range(len(psar_d)):

        if psar_u[i] == 1.0:
            
            results.append("BUY")
        elif psar_d[i] == 1.0:

            results.append("SELL")
        else:

            results.append("IGNORE")

    return results

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 9                               #
# ---------------------------------------------------------------------------- #

def forex_strategy_9(df, bb_delta=0.0001):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']

    psar_d = PSARIndicator(df['high'], df['low'], df['close']).psar_down_indicator()
    psar_u = PSARIndicator(df['high'], df['low'], df['close']).psar_up_indicator()
    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()
    results = []

    hband = BollingerBands(df['close'], 50, 3, False).bollinger_hband().dropna()
    lband = BollingerBands(df['close'], 50, 3, False).bollinger_lband().dropna()
    mband = BollingerBands(df['close'], 50, 3, False).bollinger_mavg().dropna()

    min_list = min([len(stoch_rsi_d), len(close_data), len(psar_u), len(mband)])

    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()

    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()

    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()

    psar_d = psar_d.tail(min_list).to_list()
    psar_u = psar_u.tail(min_list).to_list()

    for i in range(min_list):

        if stoch_rsi_d[i] >= 0.6:
            if psar_u[i] == 1.0:
                
                results.append("BUY")
        elif stoch_rsi_d[i] <= 0.4:
            if psar_d[i] == 1.0:

                results.append("SELL")
        else:

            results.append("IGNORE")

    return results

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 10                              #
# ---------------------------------------------------------------------------- #

def forex_strategy_10(df):#, bb_delta=0.0001):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']

    psar_d = PSARIndicator(df['high'], df['low'], df['close']).psar_down_indicator()
    psar_u = PSARIndicator(df['high'], df['low'], df['close']).psar_up_indicator()
    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()
    results = []

    hband = BollingerBands(df['close'], 50, 3, False).bollinger_hband().dropna()
    lband = BollingerBands(df['close'], 50, 3, False).bollinger_lband().dropna()
    mband = BollingerBands(df['close'], 50, 3, False).bollinger_mavg().dropna()

    min_list = min([len(stoch_rsi_d), len(close_data), len(psar_u), len(mband)])

    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()

    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()

    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()

    psar_d = psar_d.tail(min_list).to_list()
    psar_u = psar_u.tail(min_list).to_list()

    for i in range(min_list):
        delta_lband = abs(lband[i] - close_data[i])
        delta_hband = abs(hband[i] - close_data[i])
        delta_mband = abs(mband[i] - close_data[i])

        if delta_lband < delta_mband:
            if stoch_rsi_d[i] >= 0.6:
                if psar_u[i] == 1.0:
                    results.append("BUY")
        if delta_hband < delta_mband:
            if stoch_rsi_d[i] <= 0.4:
                if psar_d[i] == 1.0:
                    results.append("SELL")
            else:
                results.append("IGNORE")

    return results

# ---------------------------------------------------------------------------- #
#                                      END                                     #
# ---------------------------------------------------------------------------- #



# ---------------------------------------------------------------------------- #
#                      MACD HISTOGRAM WITH STOCHASTIC RSI                      #
# ---------------------------------------------------------------------------- #

def check_macd_with_stochrsi(df):
    # data = pandas dataframe of all indicators including close price
    # window = range/bars to look
    # rsi_delta = the error term when looking at the rsi levels.
    # between close price and ema
    # checks for stochastic crossover while above 0.8 or below 0.2
    close_data = df['close']
    # Steps:
    # 1. Check if ema is uptrend or downtrend.
    # 2. if so, look for when stochrsi <= 0.2 + rsi_delta
    # 3. confirm move with MACD histogram (diff).
    macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
    macd_histogram = macd_histogram.dropna()
    ema = ema_indicator(close_data, window=50, fillna=False).dropna()
    # Use stoch_rsi_d to search for divergences and find entry points
    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()

    #print(stoch_rsi_d)
    #ema = pd.DataFrame(ema)
    min_list = min([len(stoch_rsi_d), len(ema), len(macd_histogram), len(close_data)])
    # ---------------------------------------------------------------------------------------------------------
    close_data = close_data.tail(min_list).to_list()
    macd_histogram = macd_histogram.tail(min_list).to_list()
    ema = ema.tail(min_list).to_list()
    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()

    results = []

    for i in range(min_list):

        #first check if ema is uptrend or downtrend
        if math.exp(ema[i]-ema[i-1]) < 1:
            # if ema is increasing
            if stoch_rsi_d[i] >= 0.6:
                if macd_histogram[i] >= 0.05:
                    results.append("SELL")
                else:
                    results.append("IGNORE")
            elif stoch_rsi_d[i] <= 0.4:
                if macd_histogram[i] <= -0.05:
                    results.append("BUY")
                else:
                    results.append("IGNORE")

            else:
                results.append("IGNORE")

        elif math.exp(ema[i]-ema[i-1]) > 1:
            # if ema is decreasing
            if stoch_rsi_d[i] >= 0.6:
                if macd_histogram[i] >= 0.05:
                    results.append("SELL")
                else:
                    results.append("IGNORE")
            elif stoch_rsi_d[i] <= 0.4:
                if macd_histogram[i] <= -0.05:
                    results.append("BUY")
                else:
                    results.append("IGNORE")

            else:
                results.append("IGNORE")
        else:
            results.append("IGNORE")

    return results

# ---------------------------------------------------------------------------- #
#                        SIMPLE BOLLINGER BANDS STRATEGY 3                     #
# ---------------------------------------------------------------------------- #

def simple_bollinger_strategy_3(df):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']

    hband = BollingerBands(df['close'], 50, 3, False).bollinger_hband().dropna()
    lband = BollingerBands(df['close'], 50, 3, False).bollinger_lband().dropna()
    mband = BollingerBands(df['close'], 50, 3, False).bollinger_mavg().dropna()
    psar_d = PSARIndicator(df['high'], df['low'], df['close']).psar_down_indicator().dropna()
    psar_u = PSARIndicator(df['high'], df['low'], df['close']).psar_up_indicator().dropna()


    min_list = min([len(hband), len(psar_d), len(psar_u), len(close_data)])

    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()

    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    psar_d = psar_d.tail(min_list).to_list()
    psar_u = psar_u.tail(min_list).to_list()

    print('psar_d: %s' % psar_d)
    print('psar_u: %s' % psar_u)


    results = []

    for idx in range(len(hband)):

        delta_lband = abs(lband[idx] - close_data[idx])
        delta_hband = abs(hband[idx] - close_data[idx])
        delta_mband = abs(mband[idx] - close_data[idx])
        print('delta_lband: %s' % delta_lband)
        print('delta_hband: %s' % delta_hband)
        print('delta_mband: %s' % delta_mband)
        test = psar_d[idx] == 1.0
        print(test)
        if delta_hband < delta_mband and psar_d[idx] == 1.0:
            results.append("SELL")
            print('sell')

        elif delta_lband < delta_mband and psar_u[idx] == 1.0:
            results.append("BUY")
            print('buy')

        else:
            results.append("IGNORE")

        return results

# ---------------------------------------------------------------------------- #
#                        SIMPLE BOLLINGER BANDS STRATEGY 4                     #
# ---------------------------------------------------------------------------- #

def simple_bollinger_strategy_4(df):

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    trend = ema_trend(df, 50)

    hband = BollingerBands(df['close'], 50, 3, False).bollinger_hband().dropna()
    lband = BollingerBands(df['close'], 50, 3, False).bollinger_lband().dropna()
    mband = BollingerBands(df['close'], 50, 3, False).bollinger_mavg().dropna()

    min_list = min([len(hband), len(trend), len(close_data)])

    close_data = close_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()

    trend = trend[-min_list:]
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()

    results = []

    for idx in range(len(hband)):

        delta_lband = abs(lband[idx] - close_data[idx])
        delta_hband = abs(hband[idx] - close_data[idx])
        delta_mband = abs(mband[idx] - close_data[idx])

        if trend[idx] == "DOWNTREND":
            if delta_hband < delta_mband:
                results.append("SELL")
                print('sell')
            else:
                results.append("IGNORE")

        elif trend[idx] == "UPTREND":
            if delta_lband < delta_mband:
                results.append("BUY")
                print('buy')
            else:
                results.append("IGNORE")
        
        else:
            results.append("IGNORE")

        return results

# ---------------------------------------------------------------------------- #
#                                    MACD V2                                   #
# ---------------------------------------------------------------------------- #
def check_macd_with_stochrsiv2(df):
    # data = pandas dataframe of all indicators including close price
    # window = range/bars to look
    # rsi_delta = the error term when looking at the rsi levels.
    # between close price and ema
    # checks for stochastic crossover while above 0.8 or below 0.2
    close_data = df['close']
    # Steps:
    # 1. Check if ema is uptrend or downtrend.
    # 2. if so, look for when stochrsi <= 0.2 + rsi_delta
    # 3. confirm move with MACD histogram (diff).
    macd_histogram = macd_diff(close_data, window_slow=100, window_fast=10, window_sign=9, fillna=False)*1000
    macd_histogram = macd_histogram.dropna()
    #ema = ema_indicator(close_data, window=20, fillna=False).dropna()
    trend = ema_trend(df, 20)
    # Use stoch_rsi_d to search for divergences and find entry points
    stoch_rsi_d = StochRSIIndicator(close_data, window=100, 
        smooth1=1, smooth2=1, fillna=False).stochrsi_d()
    stoch_rsi_d = stoch_rsi_d.dropna()

    #print(stoch_rsi_d)
    #ema = pd.DataFrame(ema)
    min_list = min([len(stoch_rsi_d), len(trend), len(macd_histogram), len(close_data)])
    # ---------------------------------------------------------------------------------------------------------
    close_data = close_data.tail(min_list).to_list()
    macd_histogram = macd_histogram.tail(min_list).to_list()
    trend = trend[-min_list:]
    stoch_rsi_d = stoch_rsi_d.tail(min_list).to_list()

    results = []

    for i in range(min_list):

        #first check if ema is uptrend or downtrend
        if trend[i] == "DOWNTREND":
            # if ema is increasing
            if stoch_rsi_d[i] >= 0.6:
                if macd_histogram[i] >= 0.05:
                    results.append("SELL")
                else:
                    results.append("IGNORE")
            elif stoch_rsi_d[i] <= 0.4:
                if macd_histogram[i] <= -0.05:
                    results.append("BUY")
                else:
                    results.append("IGNORE")

            else:
                results.append("IGNORE")

        elif trend[i] == "UPTREND":
            # if ema is decreasing
            if stoch_rsi_d[i] >= 0.6:
                if macd_histogram[i] >= 0.05:
                    results.append("SELL")
                else:
                    results.append("IGNORE")
            elif stoch_rsi_d[i] <= 0.4:
                if macd_histogram[i] <= -0.05:
                    results.append("BUY")
                else:
                    results.append("IGNORE")

            else:
                results.append("IGNORE")
        else:
            results.append("IGNORE")

    return results


# ---------------------------------------------------------------------------- #
#                             SIMPLE TREND STRATEGY                            #
# ---------------------------------------------------------------------------- #

def forex_strategy_2(symbol, df):
    # stuff
    close = df['close']
    close_ema = ema_indicator(close, window=8, fillna=False).dropna()
    df_times = df.index.to_numpy()
    df_times = pd.Series(df_times).tail(len(close_ema)).to_list()
    close = close.tail(len(close_ema))

    results = []
    for idx in range(3, len(close_ema)):

        time_now = df_times[idx]
        time_now = datetime.strptime(time_now, "%Y-%m-%d %H:%M")
        from_time = time_now-timedelta(days=7)
        hourly_df = mt5.copy_rates_range(symbol, mt5.TIMEFRAME_H1, from_time, time_now)
        hourly_df = pd.DataFrame(hourly_df)
        hourly_df = hourly_df['close']
        hourly_conf = ema_indicator(hourly_df, window=8, fillna=False).dropna().tail(3).reset_index(drop=True)
        #print(hourly_conf)
        conf_bool_up = hourly_conf[0] < hourly_conf[1] < hourly_conf [2]
        conf_bool_down = hourly_conf[0] > hourly_conf[1] > hourly_conf [2]
        close_bool_up = close_ema[idx-2] < close_ema[idx-1] < close_ema[idx]
        close_bool_down = close_ema[idx] < close_ema[idx-1] < close_ema[idx-2]
        if conf_bool_up:
            if close_bool_up:
                results.append("BUY")
            else:
                results.append("IGNORE")

        elif conf_bool_down:
            if close_bool_down:
                results.append("SELL")
            else:
                results.append("IGNORE")
        else:
            results.append("IGNORE")

    return results


# ---------------------------------------------------------------------------- #
#                               FOREX STRATEGY 3                               #
# ---------------------------------------------------------------------------- #

def forex_strategy_2(symbol, df):
    # stuff
    close = df['close']
    close_ema = ema_indicator(close, window=8, fillna=False).dropna()
    df_times = df.index.to_numpy()
    df_times = pd.Series(df_times).tail(len(close_ema)).to_list()
    close = close.tail(len(close_ema))

    results = []
    for idx in range(3, len(close_ema)):

        time_now = df_times[idx]
        time_now = datetime.strptime(time_now, "%Y-%m-%d %H:%M")
        from_time = time_now-timedelta(days=7)
        hourly_df = mt5.copy_rates_range(symbol, mt5.TIMEFRAME_H1, from_time, time_now)
        hourly_df = pd.DataFrame(hourly_df)
        hourly_df = hourly_df['close']
        hourly_conf = ema_indicator(hourly_df, window=8, fillna=False).dropna().tail(3).reset_index(drop=True)
        #print(hourly_conf)
        conf_bool_up = hourly_conf[0] < hourly_conf[1] < hourly_conf [2]
        conf_bool_down = hourly_conf[0] > hourly_conf[1] > hourly_conf [2]
        close_bool_up = close_ema[idx-2] < close_ema[idx-1] < close_ema[idx]
        close_bool_down = close_ema[idx] < close_ema[idx-1] < close_ema[idx-2]
        if conf_bool_up:
            if close_bool_up:
                results.append("BUY")
            else:
                results.append("IGNORE")

        elif conf_bool_down:
            if close_bool_down:
                results.append("SELL")
            else:
                results.append("IGNORE")
        else:
            results.append("IGNORE")

    return results



# ---------------------------------------------------------------------------- #
#                          DOUBLE VWAP BOLLINGER BANDS                         #
# ---------------------------------------------------------------------------- #

def vwap_bollinger_1(df, b_band_delta=0.001):
    # optional vwap windows; 1440, 720, 360
    vwap_band = volume_weighted_average_price(df['high'], df['low'], 
        df['close'], df['tick_volume'], window=2880, fillna=True)
    vwap_band_upper_1 = vwap_band+vwap_band.std()*0.5
    vwap_band_lower_1 = vwap_band-vwap_band.std()*0.5
    vwap_band_upper_2 = vwap_band+vwap_band.std()*1
    vwap_band_lower_2 = vwap_band-vwap_band.std()*1

    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    #close_data = close_data.iloc[200:]

    # trend analysis
    trend = ema_trend(df, 50)
    hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
    list_of_lists = [len(hband), len(close_data), len(trend), len(vwap_band), len(vwap_band_lower_1)]
    #print(list_of_lists)
    min_list = min(list_of_lists)
    #print(min_list)
    close_data = close_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()


    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    #trend = pd.Series(trend).tail(min_list).to_list()
    vwap_band = vwap_band.tail(min_list).to_list()
    vwap_band_lower_1 = vwap_band_lower_1.tail(min_list).to_list()
    vwap_band_upper_1 = vwap_band_upper_1.tail(min_list).to_list()
    vwap_band_lower_2 = vwap_band_lower_2.tail(min_list).to_list()
    vwap_band_upper_2 = vwap_band_upper_2.tail(min_list).to_list()

    df = df[['open', 'high', 'low', 'close']]

    df = df.tail(min_list)
    #print(len(vwap_band))
    #print(vwap_band)
    #print(len(hband))
    #print(len(close_data))
    df['hband'] = hband
    df['lband'] = lband
    df['mband'] = mband
    df['vwap_band'] = vwap_band
    df['vwap_band_upper_1'] = vwap_band_upper_1
    df['vwap_band_lower_1'] = vwap_band_lower_1
    df['vwap_band_upper_2'] = vwap_band_upper_2
    df['vwap_band_lower_2'] = vwap_band_lower_2
    #print(df)
    #return df
    # if delta_vwap_band_upper_2
    
    #delta_lband = abs(lband[idx] - close_data[idx])
    #delta_hband = abs(hband[idx] - close_data[idx])
    #delta_mband = abs(mband[idx] - close_data[idx])
    #delta_vwap = abs(vwap_band[idx] - )
    #delta_vwap_band_lower_2 > low_data[idx] or abs(delta_vwap_band_lower_2-)

    return df
    #print(df.head())


def simple_bollinger_2(df, b_band_delta=0.0001):

    close_data = df['close']
    low_data = df['low']
    high_data = df['high']
    #close_data = close_data.iloc[200:]

    # trend analysis
    trend = ema_trend(df, 50)
    hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
    list_of_lists = [len(hband), len(close_data), len(trend)]
    #print(list_of_lists)
    min_list = min(list_of_lists)
    #print(min_list)
    close_data = close_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    trend = pd.Series(trend).tail(min_list).to_list()
    #print(len(close_data))
    #print(len(hband))
    
    #print(len(trend))
    results = []
    for i in range(min_list):
        delta_lband_low = abs(lband[i] - low_data[i])
        delta_lband = abs(lband[i] - close_data[i])

        delta_hband = abs(hband[i] - close_data[i])
        delta_hband_high = abs(hband[i] - high_data[i])
        delta_mband = abs(mband[i] - close_data[i])

        if delta_lband < delta_mband and delta_lband_low < b_band_delta or low_data[i] < lband[i]:
            results.append("BUY")
        elif delta_hband < delta_mband and delta_hband_high < b_band_delta or high_data[i] > hband[i]:
                results.append("SELL")
        else:
            results.append("IGNORE")

    return results



"""     results = []
    for idx in range(min_list):
        # Bollinger diff
        delta_lband = abs(lband[idx] - close_data[idx])
        delta_lband_low = abs(lband[idx] - low_data[idx])
        delta_hband = abs(hband[idx] - close_data[idx])
        delta_hband_high = abs(hband[idx] - high_data[idx])
        delta_mband = abs(mband[idx] - close_data[idx])
        # VWAP diff
        delta_vwap = abs(vwap_band[idx] - close_data[idx])
        delta_vwap_band_upper_1 = abs(vwap_band_upper_1[idx] - high_data[idx])
        delta_vwap_band_upper_2 = abs(vwap_band_upper_2[idx] - high_data[idx])
        delta_vwap_band_lower_1 = abs(vwap_band_lower_1[idx] - low_data[idx])
        delta_vwap_band_lower_2 = abs(vwap_band_lower_2[idx] - low_data[idx])
        
        if low_data[idx] < vwap_band_lower_2[idx] or delta_vwap_band_lower_2 < b_band_delta:
            if delta_lband_low < b_band_delta or low_data[idx] < lband[idx]:
                results.append('BUY')
            else:
                results.append('IGNORE')
        elif vwap_band_lower_2[idx] < low_data[idx] < vwap_band_lower_1[idx]:
            if delta_lband_low < b_band_delta or low_data[idx] < lband[idx]:
                results.append('BUY')
            else:
                results.append('IGNORE')

        elif high_data[idx] > vwap_band_upper_2[idx] or delta_vwap_band_upper_2 < b_band_delta:
            if delta_hband_high < b_band_delta or hband[idx] < high_data[idx]:
                results.append('SELL')
            else:
                results.append('IGNORE')

        elif vwap_band_upper_1[idx] < high_data[idx] < vwap_band_upper_2[idx]:
            if delta_hband_high < b_band_delta or hband[idx] < high_data[idx]:
                results.append('SELL')
            else:
                results.append('IGNORE')

        else:
            results.append('IGNORE')

    return results
 """

        #elif trend[idx] == 'DOWNTREND':


        #elif trend[idx] == 'SIDEWAYS':

        #else:
        #    results.append("IGNORE")

    #return results


# ---------------------------------------------------------------------------- #
#                                BACKTESTING  V2                               #
# ---------------------------------------------------------------------------- #

def no_esl(df, indicator_output, tp, sl, lot_size):
    # indicator_output = list of either buy,sell or ignore signals.
    # reverse everything so last ticker data becomes first index.
    #tp = tp/10000
    #sl = sl/10000
    # PROBLEMS WITH THIS BACKTESTER:
    # 1. CANT TELL WHETHER HIGH OR LOW WAS REACHED FIRST!!! THEREFORE CANNOT DETERMINE WHETHER TP OR SL WAS ACTIVATED!
    # cum_sl = 
    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    open_data = df['open']
    df_times = df.index.to_numpy()
    df_times = pd.Series(df_times).tail(len(indicator_output)).to_list()
    #print(indicator_output)
    
    close_data = close_data.tail(len(indicator_output)).to_list()
    high_data = high_data.tail(len(indicator_output)).to_list()
    low_data = low_data.tail(len(indicator_output)).to_list()
    open_data = open_data.tail(len(indicator_output)).to_list()

    bought_at = 0
    sold_at = 0
    results = []
    pl_results = []
    current_action = "IGNORE"
    for idx in range(len(indicator_output)-1):
        #starts from oldest data point first 
        #print(df_times[idx])
        if current_action == "BUY":
            #print("bought_at: %s" % bought_at)
            price_diff_high = (high_data[idx] - bought_at)*(10000)
            price_diff_low = (low_data[idx] - bought_at)*(10000)
            #print("Price Diff HIGH: %s" % price_diff_high)
            #print("Price Diff LOW: %s" % price_diff_low)
            # check stop loss first

            if price_diff_low <= -sl:
                loss = ((-sl/10000)*bought_amount)
                #print("LOSS: %s" % loss)
                results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                    "bought_at": bought_at, "closed_at": low_data[idx], "PL": loss})
                pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                bought_at = 0
                current_action = "IGNORE"

            elif price_diff_high >= tp:
                profit = (tp/10000)*bought_amount
                #print("PROFIT: %s" % profit)
                results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                    "bought_at": bought_at, "closed_at": high_data[idx], "PL": profit})
                pl_results.append({"time_of_close": df_times[idx],  "PL": profit})
                bought_at = 0
                current_action = "IGNORE"
        
            else:
                pl_results.append({"time_of_close": df_times[idx],  "PL": 0})
                continue



        elif current_action == "SELL":
            #print("sold_at: %s" % sold_at)
            # check sold_at target
            price_diff_high = (sold_at-high_data[idx])*(10000)
            price_diff_low =  (sold_at-low_data[idx])*(10000)
            #print("Price Diff HIGH: %s" % price_diff_high)
            #print("Price Diff LOW: %s" % price_diff_low)
            # check stop loss first
            if price_diff_high <= -sl:
                loss = ((-sl/10000)*sold_amount)
                results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                    "bought_at": sold_at, "closed_at": high_data[idx], "PL": loss})
                pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                sold_at = 0
                current_action = "IGNORE"

            elif price_diff_low >= tp:
                profit = (tp/10000)*sold_amount
                #print('profit: %s' % profit)
                results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                    "bought_at": sold_at, "closed_at": low_data[idx], "PL": profit})
                pl_results.append({"time_of_close": df_times[idx], "PL": profit})
                sold_at = 0
                current_action = "IGNORE"
            else:
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                continue
        

        else:
            pl_results.append({"time_of_close": df_times[idx], "PL": 0})
            #print("INDICATOR OUTPUT")
            if indicator_output[idx] == "BUY":
                # buy here then skip the current iteration
                #print("bought_at:%s" % bought_at)
                bought_amount = (lot_size*100000)/open_data[idx+1]
                #print("bought_amount: %s" % bought_amount)
                current_action = "BUY"
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                continue

            elif indicator_output[idx] == "SELL":
                
                # sell here then skip the current iteration
                #print("SOLD")
                #sold_at = close_data[idx]
                sold_amount = (lot_size*100000)/open_data[idx+1]
                #print("sold_amount: %s" % sold_amount)

                current_action = "SELL"
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                continue
            else:
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                continue
            
    results_df = pd.DataFrame(results)
    pl_results = pd.DataFrame(pl_results)
    pl_results.set_index('time_of_close', drop=True, inplace=True)
    return results_df, pl_results


# ---------------------------------------------------------------------------- #
#                                BACKTESTING  V2                               #
# ---------------------------------------------------------------------------- #

def no_esl_bb(df, indicator_output, tp, sl, lot_size):
    # indicator_output = list of either buy,sell or ignore signals.
    # reverse everything so last ticker data becomes first index.
    #tp = tp/10000
    #sl = sl/10000
    # PROBLEMS WITH THIS BACKTESTER:
    # 1. CANT TELL WHETHER HIGH OR LOW WAS REACHED FIRST!!! THEREFORE CANNOT DETERMINE WHETHER TP OR SL WAS ACTIVATED!
    # cum_sl = 
    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    open_data = df['open']
    df_times = df.index.to_numpy()
    df_times = pd.Series(df_times).tail(len(indicator_output)).to_list()
    #print(indicator_output)
    
    close_data = close_data.tail(len(indicator_output)).to_list()
    high_data = high_data.tail(len(indicator_output)).to_list()
    low_data = low_data.tail(len(indicator_output)).to_list()
    open_data = open_data.tail(len(indicator_output)).to_list()

    bought_at = 0
    sold_at = 0
    results = []
    pl_results = []
    current_action = "IGNORE"
    for idx in range(len(indicator_output)-1):
        #starts from oldest data point first 
        #print(df_times[idx])
        if current_action == "BUY":
            #print("bought_at: %s" % bought_at)
            price_diff_high = (high_data[idx] - bought_at)*(10000)
            price_diff_low = (low_data[idx] - bought_at)*(10000)
            #print("Price Diff HIGH: %s" % price_diff_high)
            #print("Price Diff LOW: %s" % price_diff_low)
            # check stop loss first

            if price_diff_low <= -sl:
                loss = ((-sl/10000)*bought_amount)
                #print("LOSS: %s" % loss)
                results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                    "bought_at": bought_at, "closed_at": low_data[idx], "PL": loss})
                pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                bought_at = 0
                current_action = "IGNORE"

            elif price_diff_high >= tp:
                profit = (tp/10000)*bought_amount
                #print("PROFIT: %s" % profit)
                results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                    "bought_at": bought_at, "closed_at": high_data[idx], "PL": profit})
                pl_results.append({"time_of_close": df_times[idx],  "PL": profit})
                bought_at = 0
                current_action = "IGNORE"
        
            else:
                pl_results.append({"time_of_close": df_times[idx],  "PL": 0})
                continue



        elif current_action == "SELL":
            #print("sold_at: %s" % sold_at)
            # check sold_at target
            price_diff_high = (sold_at-high_data[idx])*(10000)
            price_diff_low =  (sold_at-low_data[idx])*(10000)
            #print("Price Diff HIGH: %s" % price_diff_high)
            #print("Price Diff LOW: %s" % price_diff_low)
            # check stop loss first
            if price_diff_high <= -sl:
                loss = ((-sl/10000)*sold_amount)
                results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                    "bought_at": sold_at, "closed_at": high_data[idx], "PL": loss})
                pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                sold_at = 0
                current_action = "IGNORE"

            elif price_diff_low >= tp:
                profit = (tp/10000)*sold_amount
                #print('profit: %s' % profit)
                results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                    "bought_at": sold_at, "closed_at": low_data[idx], "PL": profit})
                pl_results.append({"time_of_close": df_times[idx], "PL": profit})
                sold_at = 0
                current_action = "IGNORE"
            else:
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                continue
        

        else:
            pl_results.append({"time_of_close": df_times[idx], "PL": 0})
            #print("INDICATOR OUTPUT")
            if indicator_output[idx][0] == "BUY":
                tp = indicator_output[idx][1]
                # buy here then skip the current iteration
                #print("bought_at:%s" % bought_at)
                bought_amount = (lot_size*100000)/open_data[idx+1]
                #print("bought_amount: %s" % bought_amount)
                current_action = "BUY"
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                continue

            elif indicator_output[idx][0] == "SELL":
                tp = indicator_output[idx][1]
                # sell here then skip the current iteration
                #print("SOLD")
                #sold_at = close_data[idx]
                sold_amount = (lot_size*100000)/open_data[idx+1]
                #print("sold_amount: %s" % sold_amount)

                current_action = "SELL"
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                continue
            else:
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                continue
            
    results_df = pd.DataFrame(results)
    pl_results = pd.DataFrame(pl_results)
    pl_results.set_index('time_of_close', drop=True, inplace=True)
    return results_df, pl_results


# ---------------------------------------------------------------------------- #
#                             BACKTESTING WITH ESL                             #
# ---------------------------------------------------------------------------- #

def with_esl(df, indicator_output, tp, sl, lot_size, loss_tolerance, skip_amount):
    # indicator_output = list of either buy,sell or ignore signals.
    # reverse everything so last ticker data becomes first index.
    #tp = tp/10000
    #sl = sl/10000
    # PROBLEMS WITH THIS BACKTESTER:
    # 1. CANT TELL WHETHER HIGH OR LOW WAS REACHED FIRST!!! THEREFORE CANNOT DETERMINE WHETHER TP OR SL WAS ACTIVATED!
    # cum_sl = 
    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    open_data = df['open']
    df_times = df.index.to_numpy()
    #print(df_times)
    df_times = pd.Series(df_times).tail(len(indicator_output)).to_list()
    #print(df_times)
    
    close_data = close_data.tail(len(indicator_output)).to_list()
    high_data = high_data.tail(len(indicator_output)).to_list()
    low_data = low_data.tail(len(indicator_output)).to_list()
    open_data = open_data.tail(len(indicator_output)).to_list()

    bought_at = 0
    sold_at = 0
    results = []
    pl_results = []
    current_action = "IGNORE"
    last_trade_result = "NONE"
    cum_sl_counter = -1
    skip = False
    for idx in range(len(indicator_output)-1):
        #starts from oldest data point first 
        #print(df_times[idx])
        if skip == True:
            skip_counter+=1
            if skip_counter >= skip_amount:
                skip = False
                continue
            else:
                continue
        else:
            if current_action == "BUY":
                price_diff_high = (high_data[idx] - bought_at)*(10000)
                price_diff_low = (low_data[idx] - bought_at)*(10000)
                # check stop loss first
                if price_diff_low <= -sl:
                    loss = (-(sl/10000)*bought_amount)
                    results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                        "bought_at": bought_at, "closed_at": low_data[idx], "PL": loss})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                    bought_at = 0
                    current_action = "IGNORE"

                    if cum_sl_counter >= loss_tolerance:
                        skip = True
                        skip_counter = 0
                        last_trade_result = "NONE"

                    if last_trade_result == "LOSS":
                        cum_sl_counter+=1
                    else:

                        last_trade_result = "LOSS"
                        cum_sl_counter+=1

                    last_trade_result == "LOSS"
                elif price_diff_high >= tp:
                    profit = ((tp/10000)*bought_amount)
                    results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                        "bought_at": bought_at, "closed_at": high_data[idx], "PL": profit})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": profit})
                    bought_at = 0
                    current_action = "IGNORE"
                    last_trade_result = "PROFIT"
                else:
                    pl_results.append({"time_of_close": df_times[idx],  "PL": 0})
                    continue



            elif current_action == "SELL":
                #print("sold_at: %s" % sold_at)
                # check sold_at target
                price_diff_high = (sold_at-high_data[idx])*(10000)
                price_diff_low =  (sold_at-low_data[idx])*(10000)
                #print("sold at price diff: %s" % price_diff)

                # check stop loss first
                if price_diff_high <= -sl:
                    loss = (-(sl/10000)*sold_amount)
                    results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                        "bought_at": sold_at, "closed_at": high_data[idx], "PL": loss})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": loss})
                    sold_at = 0
                    current_action = "IGNORE"

                    if cum_sl_counter >= loss_tolerance:
                        skip = True
                        skip_counter = 0
                        last_trade_result = "NONE"

                    if last_trade_result == "LOSS":
                        cum_sl_counter+=1

                    else:
                        last_trade_result = "LOSS"
                        cum_sl_counter+=1

                    last_trade_result = "LOSS" 

                elif price_diff_low >= tp:
                    profit = ((tp/10000)*sold_amount)
                    results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                        "bought_at": sold_at, "closed_at": low_data[idx], "PL": profit})
                    pl_results.append({"time_of_close": df_times[idx], "PL": profit})
                    sold_at = 0
                    current_action = "IGNORE"
                    last_trade_result = "PROFIT"
                # check stop loss
                else:
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    continue
            

            else:
                pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                #print("INDICATOR OUTPUT")
                if indicator_output[idx] == "BUY":
                    # buy here then skip the current iteration
                    bought_amount = (lot_size*100000)/open_data[idx+1]
                    #print("BOUGHT")
                    current_action = "BUY"
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    continue

                elif indicator_output[idx] == "SELL":
                    # sell here then skip the current iteration
                    #print("SOLD")
                    sold_amount = (lot_size*100000)/open_data[idx+1]
                    current_action = "SELL"
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    continue
                else:
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0})
                    continue
    results_df = pd.DataFrame(results)
    pl_results = pd.DataFrame(pl_results)
    pl_results.set_index('time_of_close', drop=True, inplace=True)
    return results_df, pl_results


# ---------------------------------------------------------------------------- #
#                             BACKTESTING WITH ESL                             #
# ---------------------------------------------------------------------------- #

def with_esl_risk(df, indicator_output, tp, sl, leverage, risk, balance, loss_tolerance, skip_amount):
    # indicator_output = list of either buy,sell or ignore signals.
    # reverse everything so last ticker data becomes first index.
    #tp = tp/10000
    #sl = sl/10000
    # PROBLEMS WITH THIS BACKTESTER:
    # 1. CANT TELL WHETHER HIGH OR LOW WAS REACHED FIRST!!! THEREFORE CANNOT DETERMINE WHETHER TP OR SL WAS ACTIVATED!
    # cum_sl = 
    close_data = df['close']
    high_data = df['high']
    low_data = df['low']
    open_data = df['open']
    df_times = df.index.to_numpy()
    #print(df_times)
    df_times = pd.Series(df_times).tail(len(indicator_output)).to_list()
    #print(df_times)
    margin = risk*balance
    
    close_data = close_data.tail(len(indicator_output)).to_list()
    high_data = high_data.tail(len(indicator_output)).to_list()
    low_data = low_data.tail(len(indicator_output)).to_list()
    open_data = open_data.tail(len(indicator_output)).to_list()

    bought_at = 0
    sold_at = 0
    results = []
    pl_results = []
    current_action = "IGNORE"
    last_trade_result = "NONE"
    cum_sl_counter = -1
    skip = False
    amount = margin*leverage

    for idx in range(len(indicator_output)-1):
        #starts from oldest data point first 
        #print(df_times[idx])
        if skip == True:
            skip_counter+=1
            if skip_counter >= skip_amount:
                skip = False
                continue
            else:
                continue
        else:
            if current_action == "BUY":
                price_diff_high = (high_data[idx] - bought_at)*(10000)
                price_diff_low = (low_data[idx] - bought_at)*(10000)
                # check stop loss first
                if price_diff_low <= -sl:
                    #loss = (low_data[idx] - bought_at)*(amount)
                    loss = (-(sl/10000)*bought_amount)
                    balance+=loss
                    margin = risk*balance
                    results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                        "bought_at": bought_at, "closed_at": low_data[idx], "PL": loss})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": loss, "balance": balance})
                    bought_at = 0
                    current_action = "IGNORE"

                    if cum_sl_counter >= loss_tolerance:
                        skip = True
                        skip_counter = 0
                        last_trade_result = "NONE"

                    if last_trade_result == "LOSS":
                        cum_sl_counter+=1
                    else:

                        last_trade_result = "LOSS"
                        cum_sl_counter+=1

                    last_trade_result == "LOSS"
                elif price_diff_high >= tp:
                    profit = ((tp/10000)*bought_amount)
                    balance+= profit
                    margin = risk*balance

                    results.append({"time_of_close:": df_times[idx], "trade_type": "LONG", 
                        "bought_at": bought_at, "closed_at": high_data[idx], "PL": profit})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": profit, "balance": balance})
                    bought_at = 0
                    current_action = "IGNORE"
                    last_trade_result = "PROFIT"
                else:
                    pl_results.append({"time_of_close": df_times[idx],  "PL": 0, "balance": balance})
                    continue



            elif current_action == "SELL":
                #print("sold_at: %s" % sold_at)
                # check sold_at target
                price_diff_high = (sold_at-high_data[idx])*(10000)
                price_diff_low =  (sold_at-low_data[idx])*(10000)
                #print("sold at price diff: %s" % price_diff)

                # check stop loss first
                if price_diff_high <= -sl:
                    loss = ((-sl/10000)*sold_amount)
                    balance+=loss
                    margin = risk*balance

                    results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                        "bought_at": sold_at, "closed_at": high_data[idx], "PL": loss})
                    pl_results.append({"time_of_close": df_times[idx],  "PL": loss, "balance": balance})
                    sold_at = 0
                    current_action = "IGNORE"

                    if cum_sl_counter >= loss_tolerance:
                        skip = True
                        skip_counter = 0
                        last_trade_result = "NONE"

                    if last_trade_result == "LOSS":
                        cum_sl_counter+=1

                    else:
                        last_trade_result = "LOSS"
                        cum_sl_counter+=1

                    last_trade_result = "LOSS" 

                elif price_diff_low >= tp:
                    profit = ((tp/10000)*sold_amount)
                    balance+=profit
                    margin = risk*balance

                    results.append({"time_of_close:": df_times[idx], "trade_type": "SHORT", 
                        "bought_at": sold_at, "closed_at": low_data[idx], "PL": profit})
                    pl_results.append({"time_of_close": df_times[idx], "PL": profit, "balance": balance})
                    sold_at = 0
                    current_action = "IGNORE"
                    last_trade_result = "PROFIT"
                # check stop loss
                else:
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0, "balance": balance})
                    continue
            

            else:
                pl_results.append({"time_of_close": df_times[idx], "PL": 0, "balance": balance})
                #print("INDICATOR OUTPUT")
                if indicator_output[idx] == "BUY":
                    # buy here then skip the current iteration
                    bought_at = open_data[idx+1]
                    #print("BOUGHT")
                    current_action = "BUY"
                    margin = risk*balance
                    bought_amount = (margin*leverage)/open_data[idx+1]
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0, "balance": balance})
                    continue

                elif indicator_output[idx] == "SELL":
                    # sell here then skip the current iteration
                    #print("SOLD")
                    sold_at = open_data[idx+1]
                    current_action = "SELL"
                    margin = risk*balance
                    sold_amount = (margin*leverage)/open_data[idx+1]
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0, "balance": balance})
                    continue
                else:
                    pl_results.append({"time_of_close": df_times[idx], "PL": 0, "balance": balance})
                    continue

    results_df = pd.DataFrame(results)
    pl_results = pd.DataFrame(pl_results)
    pl_results.set_index('time_of_close', drop=True, inplace=True)
    return results_df, pl_results



def simple_bollinger_2(df, b_band_delta=0.0001):

    close_data = df['close']
    low_data = df['low']
    high_data = df['high']
    #close_data = close_data.iloc[200:]

    # trend analysis
    trend = ema_trend(df, 50)
    hband = BollingerBands(close_data, 50, 3, False).bollinger_hband()
    lband = BollingerBands(close_data, 50, 3, False).bollinger_lband()
    mband = BollingerBands(close_data, 50, 3, False).bollinger_mavg()
    list_of_lists = [len(hband), len(close_data), len(trend)]
    #print(list_of_lists)
    min_list = min(list_of_lists)
    #print(min_list)
    close_data = close_data.tail(min_list).to_list()
    low_data = low_data.tail(min_list).to_list()
    high_data = high_data.tail(min_list).to_list()
    hband = hband.tail(min_list).to_list()
    lband = lband.tail(min_list).to_list()
    mband = mband.tail(min_list).to_list()
    trend = pd.Series(trend).tail(min_list).to_list()
    #print(len(close_data))
    #print(len(hband))
    
    #print(len(trend))
    results = []
    for i in range(min_list):
        delta_lband_low = abs(lband[i] - low_data[i])
        delta_lband = abs(lband[i] - close_data[i])

        delta_hband = abs(hband[i] - close_data[i])
        delta_hband_high = abs(hband[i] - high_data[i])
        delta_mband = abs(mband[i] - close_data[i])
        delta_bband = abs(hband[i] - lband[i])/2
        delta_bband = round(delta_bband, 2)
        if delta_lband < delta_mband and delta_lband_low < b_band_delta or low_data[i] < lband[i]:
            results.append("BUY")
        elif delta_hband < delta_mband and delta_hband_high < b_band_delta or high_data[i] > hband[i]:
                results.append("SELL")
        else:
            results.append("IGNORE")

    return results


# ---------------------------------------------------------------------------- #
#                               TEST SYMBOL LIST                               #
# ---------------------------------------------------------------------------- #
""" 
symbols_list = ["EURUSD", "AUDNZD", "GBPUSD", "GBPCHF", "USDCAD", "AUDUSD", "USDCHF", "EURCHF", "NZDCAD", "EURCAD"]
#symbols_list = ["USDJPY", "GBPJPY", "CADJPY", "AUDJPY"]
winning_pairs = []
timezone = pytz.timezone("EET")
strategy = "check_macd_with_stochrsi"
tp = 3
sl = 9
for symbol in symbols_list:
    print('----------------------------------')
    print("NOW TESTING '%s' CURRENCY PAIR FOR %s" % (symbol, strategy))
    print('----------------------------------')
    # STILL NEED TO FIX BUY/SELL SIGNAL!!!!!!!!!!!!!!!!!!!
    eet_from = datetime(2021, 1, 1, tzinfo=timezone)
    df = get_data(symbol, eet_from, mt5.TIMEFRAME_M5)
    #print(df)
    #df_anz = get_data("AUDNZD", eet_from, mt5.TIMEFRAME_M1)

    #output_list = atr_trend(df, 10, 3)
    #output_list = atr_trendv2(df, 10, 3)
    #output_list = stochrsi_macd_bollinger_indicator(df, b_band_delta=0.0001)
    #output_list = forex_strategy_1(symbol, df)
    #output_list = forex_strategy_2(symbol, df)
    #output_list = forex_strategy_3(df)
    #output_list = forex_strategy_4(df)
    #output_list = forex_strategy_5(df)
    #output_list = forex_strategy_6(df)
    #output_list = forex_strategy_7(df)

    #output_list = stochrsi_macd_bollinger_indicatorv2(df, b_band_delta=0.001)
    
    output_list = check_macd_with_stochrsi(df)
    #results, pl_results = backtest_indicator_no_esl(df, output_list, tp, sl, 0.01)

    results, pl_results = backtest_indicator_with_esl(df, output_list, tp, sl, 0.01, 1, 10)
    #results, pl_results = backtest_indicator_with_eslv2(df, output_list, 5, 3, 0.01)
    #print(results)
    #df_times = df.index.to_numpy()    
    #df_times = pd.Series(df_times).to_list()

    #print('----------------------------------')
    description = describe_results(results)
    #for key, value in description.items():
    #    print("%s: %s" % (key, value))
    expected_win_rate = round(description['win_rate']*100, 2)
    asdf = len(results)*round(description['win_rate'], 4)
    print("number of win trade average: %s" % asdf)
    #win_rate = round(description['win_rate'], 2)
    total_pl = round(description['total_PL'], 2)
    winning_pairs.append((symbol, expected_win_rate, total_pl))

    #print('----------------------------------')
    #print("from date: %s" % df_times[0])
    #print("to date: %s" % df_times[-1])

    calculate_profitability(tp, sl, description, win_error=0.05)

print(winning_pairs)
 """


# ---------------------------------------------------------------------------- #
#                                    TESTING                                   #
# ---------------------------------------------------------------------------- #

symbol = "EURUSD"
timezone = pytz.timezone("EET")
# create 'datetime' object in EET time zone to avoid the implementation of a local time zone offset
eet_from = datetime(2021, 11, 30, tzinfo=timezone)
eet_to = datetime.now(timezone)

# get 10 EURUSD H4 bars starting from 01.10.2020 in EET time zone
#rates = mt5.copy_rates_range("EURUSD", mt5.TIMEFRAME_M5, eet_from, eet_to)
df = get_data(symbol, eet_from, mt5.TIMEFRAME_M1)


tp = 6
sl = 3

# ---------------------------------------------------------------------------- #
#                              TEST STRATEGY HERE                              #
# ---------------------------------------------------------------------------- #

#output_list = forex_strategy_1(symbol, df)
#output_list = forex_strategy_2(symbol, df)
#output_list = forex_strategy_3(df)
#output_list = forex_strategy_4(df)
#output_list = forex_strategy_5(df)
#output_list = forex_strategy_6(df)
#output_list = forex_strategy_7(df)
#output_list = check_macd_with_stochrsi(df)
#output_list = check_macd_with_stochrsiv2(df)
#output_list = stochrsi_macd_bollinger_indicatorv2(df, b_band_delta=0.0001)
#output_list = stochrsi_macd_bollinger_indicator(df, b_band_delta=0.0001)
#output_list = simple_bollinger_strategy_1(df, 0.0001)
#output_list = simple_bollinger_strategy_2(df, 0.0001)
#output_list = forex_strategy_8(df)
#output_list = forex_strategy_9(df)
#output_list = forex_strategy_10(df)
#output_list = simple_bollinger_strategy_3(df)
#output_list = simple_bollinger_strategy_4(df)
#output_list = forex_strategy_1(df)
#output_list = simple_bollinger_1(df)

output_list = simple_bollinger_2(df)

strategy = "simple_bollinger_2"
results, pl_results = no_esl(df, output_list, tp, sl, 0.01)
#print(results)
#print(pl_results)
#results, pl_results = backtest_indicator_with_esl(df, output_list, tp, sl, 0.01, 100, 2, 20)
#results, pl_results = backtest_indicator_with_esl_and_spread(df, output_list, tp, sl, 0.01, 2, 15, 1)
#results, pl_results = with_esl_mtg(df, output_list, tp, sl, 0.01, 1, 10)
#results, pl_results = with_esl_risk(df, output_list, tp, sl, 2000, 0.05, 10, 2, 20)
#results, pl_results = with_esl(df, output_list, tp, sl, 0.01, 1, 20)

#df2 = pd.DataFrame(results)
#df2.to_csv("results.csv", index=False)



df_times = df.index.to_numpy()    
df_times = pd.Series(df_times).to_list()

print('----------------------------------')
description = describe_results(results)
for key, value in description.items():
    print("%s: %s" % (key, value))
#expected_win_rate = round(description['win_rate']*100, 4)
print('----------------------------------')
print("from date: %s" % df_times[0])
print("to date: %s" % df_times[-1])

calculate_profitability(tp, sl, description, win_error=0.1)


# ---------------------------------------------------------------------------- #
#                                 PLOT RESULTS                                 #
# ---------------------------------------------------------------------------- #


pl_results['cumsum'] = pl_results['PL'].cumsum()#/80
pl_results['cumsum'] = pl_results['cumsum'].fillna(value=0)
pl_results['ema'] = ema_indicator(pl_results['PL'], 60)
df_times = df.index.to_numpy()    
df_times = pd.Series(df_times).to_list()
#print(pl_results.head(60))
#print(pl_results.tail())
#trace1 = go.Scatter(x=pl_results.index.to_numpy(), y = pl_results['balance'], text = 'Account Balance')
trace2 = go.Scatter(x=pl_results.index.to_numpy(), y = pl_results['cumsum'], text = 'Cumulative Profits')


#trace3 = go.Scatter(x=pl_results.index.to_numpy(), y = pl_results['ema'])
#trace = go.Scatter(x=df.index.to_numpy(), y=df['tick_volume'])
num_trades = description['total_trades']
expected_win_rate = round(description['win_rate']*100, 4)

layout = go.Layout(
    title = "TEST VWAP EURUSD",
    xaxis = {'title' : 'Time in Minutes'},
    yaxis = {'title' : 'Price'} #% pl_results['balance'][0]}
)


layout = go.Layout(
    title = "%s Expected Profits, Strategy: %s, Expected Win Rate: %s Percent" % (symbol, strategy, expected_win_rate),
    xaxis = {'title' : 'Time in Minutes, Total Number of Trades: %s' % num_trades},
    #yaxis = {'title' : 'Starting Balance: $%s, TP: %s, SL: %s' % (pl_results['balance'][0], tp, sl)}
    yaxis = {'title' : 'Starting Balance: $%s, TP: %s, SL: %s' % (10, tp, sl)}
)

fig = go.Figure(data = [trace2], layout=layout)
fig.show()


