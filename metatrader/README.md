# MetaTrader

A collection of test scripts and live bot scripts for forex trading.

The user simply needs to add; account name, server number, and password for their MetaTrader account.


## Trading Bot Features

- Uses an emergency stop loss with a cooldown period.

- Uses multiple strategies such as; stochastic RSI, MACD, Bollinger Bands and VWAP.

## Test Forex Strategy Features

- Default timezone is EET

- Can test across multiple symbols

- Uses multiple strategies such as; stochastic RSI, MACD, Bollinger Bands and VWAP.

### Instructions for trading_bot.py

- Change "server_name" to the string name of the server of the account

- "server_num" and "account" are both the same number which is the account number.

- "password" is the password of the metatrader account.

- to set custom take profit and stop loss in pips, change "tp" and "sl". The value is ####NOT in micro-pips!

### Instructions for test_forex_strategy.py

- Change "symbol" to the forex symbol you are testing

- Change "server_name" to the string name of the server of the account

- "server_num" and "account" are both the same number which is the account number.

- "password" is the password of the metatrader account.

### List of Strategies Available for trading_bot.py

- Stochastic RSI

- MACD

- Bollinger Bands

- VWAP

### List of Strategies Available for test_forex_strategy.py


- Stochastic RSI

- MACD

- Bollinger Bands

- VWAP

- ATR
