# 5 minute scalping strategy using 8 & 21 Exponential Moving Averages
# Input = ticker code, Output = When to Buy/Sell

# Variables Definitions:
# 
# from_ticker: currency code from which to convert.
# to_ticker: currency code to convert to.
# ticker_interval: string of interval times listed by alpha_vantage
# e.g. '1min', '5min', etc...
#
# ticker_list: list of tuples each containing form_ticker and
# to_ticker; e.g. [('CNY', 'USD'), ('GBP', 'AUD')...]



from alpha_vantage.foreignexchange import ForeignExchange
import decimal
import numpy as np
import pandas as pd
import math
import sys

fx = ForeignExchange(key='EDBLD2ZU2FGQHBGF', output_format='pandas')

ticker_info = pd.DataFrame([['confirmation_interval', '15min', '60min', 'daily'],
    ['ticker_interval_length', 720, 1440, 1440],
    ['conf_interval_length', 96, 24, 60]], columns = ['ticker_interval', '1min', '5min', '60min'])

ticker_info.set_index('ticker_interval', inplace=True)

#print(ticker_info.at['confirmation_interval', '5min'])

class fx_ticker_pair:

    def __init__(self, from_ticker, to_ticker, ticker_interval, output_size):

        self.from_ticker = from_ticker
        self.to_ticker = to_ticker
        self.ticker_interval = ticker_interval
        self.output_size = output_size

    # Returns a list of intraday close values for a given ticker pair.
    def intraday_pair_data(self):

        tmp, _ = fx.get_currency_exchange_intraday(
                from_symbol=self.from_ticker, to_symbol=self.to_ticker, 
                interval=self.ticker_interval, outputsize=self.output_size)

        return tmp['4. close'].to_list()

    # Returns a list of daily close values for a given ticker pair.
    def daily_pair_data(self):

        tmp, _ = fx.get_currency_exchange_daily(
                from_symbol=self.from_ticker, to_symbol=self.to_ticker, 
                outputsize=self.output_size)

        return tmp['4. close'].to_list()







#print(len(ticker_pair('CNY', 'USD', '1min', 'full').pair_data()))

class fx_tickers:

    def __init__(self, ticker_list, ticker_interval, output_size):
        self.ticker_list = ticker_list
        self.ticker_interval = ticker_interval
        self.output_size = output_size

    def get_fx_data(self):
        ticker_data = pd.DataFrame([])
        confirmation_data = pd.DataFrame([])

        # Ticker Interval = '1min'
        if self.ticker_interval == '1min':


            for i in self.ticker_list:

                # Returns list of values using fx_ticker_pair class
                tmp_conf_values = fx_ticker_pair(i[0], i[1], 
                        ticker_info.at['confirmation_interval', '1min'],
                        self.output_size).intraday_pair_data()

                tmp_values = fx_ticker_pair(i[0], i[1], self.ticker_interval,
                        self.output_size).intraday_pair_data()
               

                # Add to dataframes here
                confirmation_data[i[0]+i[1]] = tmp_conf_values[-96:]

                ticker_data[i[0]+i[1]] = tmp_values[-720:]

        elif self.ticker_interval == '5min':

            for i in self.ticker_list:
  
                # Returns list of values using fx_ticker_pair class
                tmp_conf_values = fx_ticker_pair(i[0], i[1], ticker_info.at['confirmation_interval', '5min'],
                        self.output_size).intraday_pair_data()

                tmp_values = fx_ticker_pair(i[0], i[1], self.ticker_interval,
                        self.output_size).intraday_pair_data()
  

                # Add to dataframes here
                confirmation_data[i[0]+i[1]] = tmp_conf_values[-24:]

                ticker_data[i[0]+i[1]] = tmp_values[-720:]

 
        
        elif self.ticker_interval == '60min':

            for i in self.ticker_list:

                # Returns list of values using fx_ticker_pair class
                tmp_conf_values = fx_ticker_pair(i[0], i[1], 
                        ticker_info.at['confirmation_interval', '60min'],
                        self.output_size).intraday_pair_data()

                tmp_values = fx_ticker_pair(i[0], i[1], self.ticker_interval,
                        self.output_size).daily_pair_data()


                # Add to dataframes here
                confirmation_data[i[0]+i[1]] = tmp_conf_values[-60:]

                ticker_data[i[0]+i[1]] = tmp_values[-720:]


        else:
            return print("Error: Only supports '1min' and '5min' ticker_interval")


        return confirmation_data, ticker_data


#conf_data, code_data = fx_tickers([('CNY', 'USD'), ('GBP', 'AUD')], '5min', 'full').get_fx_data() 

